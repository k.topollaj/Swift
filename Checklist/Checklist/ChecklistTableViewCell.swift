//
//  ChecklistTableViewCell.swift
//  Checklist
//
//  Created by Kevin Topollaj on 02/10/2018.
//  Copyright © 2018 Kevin Topollaj. All rights reserved.
//

import UIKit

class ChecklistTableViewCell: UITableViewCell {

    // Outlets
    @IBOutlet weak var checkmarkLabel: UILabel!
    @IBOutlet weak var todoTextLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
