//
//  ChecklistItem.swift
//  Checklist
//
//  Created by Kevin Topollaj on 01/10/2018.
//  Copyright © 2018 Kevin Topollaj. All rights reserved.
//

// MODEL for the app

import Foundation

class ChecklistItem: NSObject{
    
    // Properties
    
    @objc var text = ""
    var checked = false
    
    // Methods
    
    func toggleChecked(){
        checked = !checked
    }
}
