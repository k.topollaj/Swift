//
//  TodoList.swift
//  Checklist
//
//  Created by Kevin Topollaj on 01/10/2018.
//  Copyright © 2018 Kevin Topollaj. All rights reserved.
//

// Contains all our check list items
import Foundation

class TodoList {
    
    // Create an enum for the priorities tha you can loop through all the elements (CaseIterable)
    enum Priority: Int, CaseIterable {
        case high, medium, low, no
    }
    
    // Properties
    private var highPriorityTodos: [ChecklistItem] = []
    private var mediumPriorityTodos: [ChecklistItem] = []
    private var lowPriorityTodos: [ChecklistItem] = []
    private var noPriorityTodos: [ChecklistItem] = []
    
    
    init() {
        // create an instance of the ChecklistItem
        let row0Item = ChecklistItem()
        row0Item.text = "Learn Collection View"
        
        let row1Item = ChecklistItem()
        row1Item.text = "Read iOS Book"
        
        let row3Item = ChecklistItem()
        row3Item.text = "Read iOS Documentation"
        
        let row4Item = ChecklistItem()
        row4Item.text = "Plan Vacations"
        
        // add them in to the array
        addTodo(row0Item, for: .high)
        addTodo(row1Item, for: .medium)
        addTodo(row3Item, for: .low)
        addTodo(row4Item, for: .no)
    }
    
    // adds a todo in the check list
    func addTodo(_ item: ChecklistItem, for priority: Priority, at index: Int = -1) {
        switch priority {
        case .high:
            if index < 0 {
                highPriorityTodos.append(item)
            } else {
                highPriorityTodos.insert(item, at: index)
            }
        case .medium:
            if index < 0 {
                mediumPriorityTodos.append(item)
            } else {
                mediumPriorityTodos.insert(item, at: index)
            }
        case .low:
            if index < 0 {
                lowPriorityTodos.append(item)
            } else {
                lowPriorityTodos.insert(item, at: index)
            }
        case .no:
            if index < 0 {
                noPriorityTodos.append(item)
            } else {
                noPriorityTodos.insert(item, at: index)
            }
        }
    }
    
    // create a todo list based on the priority
    func todoList(for priority: Priority) -> [ChecklistItem] {
        switch priority {
        case .high:
            return highPriorityTodos
        case .medium:
            return mediumPriorityTodos
        case .low:
            return lowPriorityTodos
        case .no:
            return noPriorityTodos
        }
    }
    
    
    func newTodo() -> ChecklistItem {
        // generate a new check list item
        let item = ChecklistItem()
        item.text = randomTitle()
        // every new item that we add will be checked
        item.checked = true
        // add that to the mediumPriorityTodos array by default
        mediumPriorityTodos.append(item)
        return item
    }
    
    func move(item: ChecklistItem,
              from sourcePriority: Priority,
              at sourceIndex: Int,
              to destinationPriority: Priority,
              at destinationIndex: Int)
    {
        // remove the item
        remove(item, from: sourcePriority, at: sourceIndex)
        // add todo
        addTodo(item, for: destinationPriority, at: destinationIndex)
    }
    
    
    // remove one item at a time from a specific priority
    func remove(_ item: ChecklistItem, from priority: Priority, at index: Int){
        switch priority {
        case .high:
            highPriorityTodos.remove(at: index)
        case .medium:
            mediumPriorityTodos.remove(at: index)
        case .low:
            lowPriorityTodos.remove(at: index)
        case .no:
            noPriorityTodos.remove(at: index)
        }
    }
    
    // create random text for each check mark item
    private func randomTitle() -> String {
        var titles = ["New to do item", "Generic to do", "Fill me out"]
        let randomNumber = Int.random(in: 0 ... titles.count - 1)
        return titles[randomNumber]
    }
    
}
