//
//  ViewController.swift
//  Checklist
//
//  Created by Kevin Topollaj on 01/10/2018.
//  Copyright © 2018 Kevin Topollaj. All rights reserved.
//

import UIKit

class ChecklistViewController: UITableViewController {
    
    // Properties
    var todoListInstance: TodoList
    
    // will cast a number into an enumeration
    private func priorityForSectionIndex(_ index: Int) -> TodoList.Priority? {
        return TodoList.Priority(rawValue: index)
    }
    
    
    @IBAction func addItem(_ sender: Any) {
        
        let newRowIndex = todoListInstance.todoList(for: .medium).count
        _ = todoListInstance.newTodo()
        
        let indexPath = IndexPath(row: newRowIndex, section: 0)
        tableView.insertRows(at: [indexPath], with: .automatic)
    
    }
    
    @IBAction func deleteItems(_ sender: Any) {
        // get a list of the selected rows
        if let selectedRows = tableView.indexPathsForSelectedRows {
            // loop through the selectedRow
            for indexPath in selectedRows {
                if let priority = priorityForSectionIndex(indexPath.section){
                    // get todos based on the priority
                    let todos = todoListInstance.todoList(for: priority)
                    // row to delete
                    let rowToDelete = indexPath.row > todos.count - 1 ? todos.count - 1 : indexPath.row
                    // get the item
                    let item = todos[rowToDelete]
                    // remove an item from the to do list
                    todoListInstance.remove(item, from: priority, at: rowToDelete)
                }
            }
            // update the table view
            tableView.beginUpdates()
            tableView.deleteRows(at: selectedRows, with: .automatic)
            tableView.endUpdates()
        }
    }
    
    // Initializer : is called when the view controller is initialized from the story board
    required init?(coder aDecoder: NSCoder) {
        // create a new TodoList Instance
        todoListInstance = TodoList()
        
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Access NavigationController from code
        navigationController?.navigationBar.prefersLargeTitles = true
        
        // add an edit bar button in the left side of the navigation
        navigationItem.leftBarButtonItem = editButtonItem
        // enable multiple selection in editing
        tableView.allowsMultipleSelectionDuringEditing = true
        
    }
    
    // call the set editing method
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: true)
        // call set editing on the table view
        tableView.setEditing(tableView.isEditing, animated: true)
    }
    
    // number of rows to display
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let priority = priorityForSectionIndex(section){
            return todoListInstance.todoList(for: priority).count
        }
        return 0
    }
    
    // when Table View needs a cell
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // provide the reusable cell
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChecklistItem", for: indexPath)
        
        // get the priority
        if let priority = priorityForSectionIndex(indexPath.section){
            let items = todoListInstance.todoList(for: priority)
            let item = items[indexPath.row]
            configureText(for: cell, with: item)
            configureCheckmark(for: cell, with: item)
        }
        
        return cell 
    }
    
    // implement a delegate method for the interaction when a uset taps on a row
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // if the table is in editing mode do nothing
        if tableView.isEditing {
            return
        }
        
        // get the current cell by using the indexPath
        if let cell = tableView.cellForRow(at: indexPath){
            if let priority = priorityForSectionIndex(indexPath.section){
                let items = todoListInstance.todoList(for: priority)
                let item = items[indexPath.row]
                // toggle the check mark
                item.toggleChecked()
                
                configureCheckmark(for: cell, with: item)
                tableView.deselectRow(at: indexPath, animated: true)
            }
        }
    }
    
    // Adding delete to the table view cells
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if let priority = priorityForSectionIndex(indexPath.section){
            let item = todoListInstance.todoList(for: priority)[indexPath.row]
            todoListInstance.remove(item, from: priority, at: indexPath.row)
            // tell the table the item that was deleted
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    // Method used for moving rows in the table view
    override func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        // move the row
        
        // get the source priority and the destination priority
        if let srcPriority = priorityForSectionIndex(sourceIndexPath.section),
            let destPriority = priorityForSectionIndex(destinationIndexPath.section){
            // get the item that you are moving
            let item = todoListInstance.todoList(for: srcPriority)[sourceIndexPath.row]
            todoListInstance.move(item: item, from: srcPriority, at: sourceIndexPath.row, to: destPriority, at:  destinationIndexPath.row)
        }
        // update the data in the model
        tableView.reloadData()
    }
    
    // Methods
    
    func configureText(for cell: UITableViewCell, with item: ChecklistItem){
        if let checkmarkCell = cell as? ChecklistTableViewCell {
            // it will put the text in to the label
            checkmarkCell.todoTextLabel.text = item.text
        }
    }
    
    
    func configureCheckmark(for cell: UITableViewCell, with item: ChecklistItem) {
        // get the checkmark cell by casting it to ChecklistTableViewCell
        guard let checkmarkCell = cell as? ChecklistTableViewCell else {
            return
        }
        
        // check the element based on the row nr if they are checked
        if item.checked {
                checkmarkCell.checkmarkLabel.text = "√"
        } else {
                checkmarkCell.checkmarkLabel.text = ""
        }
    }
    
    // Become a delegate of the AddItemViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         // check the segue identifier
        if segue.identifier == "AddItemSegue"{
            // get a reference of AddItemTableViewController
            if let addItemViewController = segue.destination as? ItemDetailViewController {
                // access the delegate property and asign it to self
                addItemViewController.delegate = self
                addItemViewController.todoList = todoListInstance
            }
        } else if segue.identifier == "EditItemSegue" {
            // get the destination ViewController
            if let addItemViewController = segue.destination as? ItemDetailViewController{
                // get the item that we are editing and get the index path for that cell in the table view
                if let cell = sender as? UITableViewCell, let indexPath = tableView.indexPath(for: cell), let priority = priorityForSectionIndex(indexPath.section) {
                    // get the item
                    let item = todoListInstance.todoList(for: priority)[indexPath.row]
                    // add the item
                    addItemViewController.itemToEdit = item
                    // assign a delegate
                    addItemViewController.delegate = self
                }
            }
        }
    }
    
    // get the nr of sections
    override func numberOfSections(in tableView: UITableView) -> Int {
        return TodoList.Priority.allCases.count
    }
    
    // create section for each title
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        var title: String? = nil
        // get the priority
        if let priority = priorityForSectionIndex(section){
            switch priority {
            case .high:
                title = "High Priority Todo"
            case .medium:
                title = "Medium Priority Todo"
            case .low:
                title = "Low Priority Todo"
            case .no:
                title = "Someday Priority Todo"
            }
        }
        return title
    }
}

// implement the new Protocol
extension ChecklistViewController: ItemDetailViewControllerDelegate {
    
    func addItemViewControllerDidCancel(_ controller: ItemDetailViewController) {
        // pop the View Controller of the stack
        navigationController?.popViewController(animated: true)
    }
    
    func addItemViewController(_ controller: ItemDetailViewController, didFinishAdding item: ChecklistItem) {
        // pop the View Controller of the stack
        navigationController?.popViewController(animated: true)
        
        // get the current amount of the items tha are in the todo list
        let rowIndex = todoListInstance.todoList(for: .medium).count - 1
        // create an index path
        let indexPath = IndexPath(row: rowIndex, section:  TodoList.Priority.medium.rawValue)
        // update the table view
        tableView.insertRows(at: [indexPath], with: .automatic)
    }
    
    func addItemViewController(_ controller: ItemDetailViewController, didFinishEditing item: ChecklistItem) {
        // find at what priority this checklist item is located
        for priority in TodoList.Priority.allCases {
            let currentList = todoListInstance.todoList(for: priority)
            // if the item is in the list it will return an index
            if let index = currentList.index(of: item){
                // create an index path
                let indexPath = IndexPath(row: index, section: priority.rawValue)
                // get the cell
                if let cell = tableView.cellForRow(at: indexPath){
                    // configure the Text
                    configureText(for: cell, with: item)
                }
            }
        }
        
        // remove the view controller when it is done
        navigationController?.popViewController(animated: true)
    }
    
}
