//
//  AddItemTableViewController.swift
//  Checklist
//
//  Created by Kevin Topollaj on 01/10/2018.
//  Copyright © 2018 Kevin Topollaj. All rights reserved.
//

import UIKit

// create a Protocol that will only work on clases
protocol AddItemViewControllerDelegate: class {
    // when user cancels the proces of adding an item to the check list
    func addItemViewControllerDidCancel(_ controller: ItemDetailView)
    // when the user adds a new item to the check list
    func addItemViewController(_ controller: ItemDetailView, didFinishAdding item: ChecklistItem)
    // when the user edit an item
    func addItemViewController(_ controller: ItemDetailView, didFinishEditing item: ChecklistItem)
}


class ItemDetailView: UITableViewController {
    
    // Create a delegate property
    weak var delegate: AddItemViewControllerDelegate?
    
    // create properties on the receiving view controller
    weak var todoList: TodoList?
    weak var itemToEdit: ChecklistItem?
    
    // MARK: - Outlets
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var addBarButton: UIBarButtonItem!
    @IBOutlet weak var cancelBarButton: UIBarButtonItem!
    
    // MARK: - Actions
    @IBAction func cancel(_ sender: Any) {
        // use the first delegate method
        delegate?.addItemViewControllerDidCancel(self)
    }
    
    @IBAction func done(_ sender: Any) {
        
        if let item = itemToEdit, let text = textField.text {
            // update the text
            item.text = text
            // call the delegate method
            delegate?.addItemViewController(self, didFinishEditing: item)
            
        } else {
            // create a new check list item
            if let item = todoList?.newTodo() {
                // get the text field text and asign it to the new check list item
                if let textFieldText = textField.text {
                    item.text = textFieldText
                }
                item.checked = false
                // call the delegate ot add the new item in to the check list
                delegate?.addItemViewController(self, didFinishAdding: item)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // check if we are editing
        if let item = itemToEdit {
            title = "Edit Item"
            textField.text = item.text
            addBarButton.isEnabled = true
        }

        navigationItem.largeTitleDisplayMode = .never
    
        // set the delegate of the text field as itself
        textField.delegate = self
    }
    
    // show the keyboard when the user taps on the add button
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // set the text field as the firs responder
        textField.becomeFirstResponder()
    }

    // Determines if a row ca be selected or not
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return nil
    }
}

// implement a text field protocol to make it resig as a first responder when the user its done
extension ItemDetailView: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // dismis the keyboard
        textField.resignFirstResponder()
        return false
    }
    
    // is called when a user taps a key on the keyboard
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        // check if the user has typed in the text field
        
        // if he has not typed return false
        guard let oldText = textField.text, let stringRange = Range(range, in: oldText) else {
            return false
        }
        
        // get the new text
        let newText = oldText.replacingCharacters(in: stringRange, with: string)
        // check if the new text is empty
        if newText.isEmpty {
            addBarButton.isEnabled = false
        } else {
            addBarButton.isEnabled = true
        }
        return true
    }
}
