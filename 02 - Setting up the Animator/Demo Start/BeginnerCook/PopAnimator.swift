import Foundation
import UIKit

// Will use the protocol to turn it to the animator for the transition
class PopAnimator: NSObject, UIViewControllerAnimatedTransitioning {

    let duration = 1.0
    var originFrame = CGRect.zero

    // check the presentin animation
    var presenting = true
    
    // 1. First method that will specify the duration of the animation
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }
    
    // 2. Second method will define the animation
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        // 1. Set up transition
        // define the view you want to animate and the state they are
        let containerView =  transitionContext.containerView
        let toView = transitionContext.view(forKey: .to)!
        
        //the view that you always want animated
        let herbView = presenting ? toView : transitionContext.view(forKey: .from)!
        
        // define the initial and final frame for herbView
        // if it is animating it will be the originFrame and if it dismising will be the herbView frame
        let initialFrame = presenting ? originFrame : herbView.frame
        // will do the oposite
        let finalFrame = presenting ? herbView.frame : originFrame
        
        // For close scale animation
        let xScaleFactor = presenting ? initialFrame.width / finalFrame.width : finalFrame.width / initialFrame.width
        let yScaleFactor = presenting ? initialFrame.height / finalFrame.height : finalFrame.height / initialFrame.height
        
        // calculate the scale transform
        let scaleFactor = CGAffineTransform(scaleX: xScaleFactor, y: yScaleFactor)
        
        if presenting {
            herbView.transform = scaleFactor
            herbView.center = CGPoint(x: initialFrame.midX, y: initialFrame.midY)
        }
        
        // adjust the corner radius in animation
        herbView.layer.cornerRadius = self.presenting ? 20.0 / xScaleFactor : 0.0
        herbView.clipsToBounds = true
        
        // add herbView to containerView
        containerView.addSubview(toView)
        containerView.bringSubview(toFront: herbView)
        
        // access the HerbDetailsViewController
        let herbController = transitionContext.viewController(forKey: presenting ? .to : .from) as! HerbDetailsViewController
        
        // fade container in when prsenting and fade it out when the presentatin has ended
        if presenting {
            herbController.containerView.alpha = 0.0
        }
        
        // 2. Animate
        UIView.animate(
            withDuration: duration,
            delay: 0.0,
            usingSpringWithDamping: 0.4,
            initialSpringVelocity: 0.0,
            animations: {
                herbView.layer.cornerRadius = self.presenting ? 0.0 : 20.0 / xScaleFactor
                herbView.transform = self.presenting ? .identity : scaleFactor
                herbView.center = CGPoint(x: finalFrame.midX, y: finalFrame.midY)
                herbController.containerView.alpha = self.presenting ? 1.0 : 0.0
            },
            completion: {_ in
                // 3. Complete Transition
                
                // after the presentation is done show the selectedImage
                if !self.presenting {
                    let viewController = transitionContext.viewController(forKey: .to) as! ViewController
                    viewController.selectedImage?.isHidden = false
                }
                
                transitionContext.completeTransition(true)
            }
        )
        
        
    }
    
}
