//
//  ViewController.swift
//  BullsEye
//
//  Created by Kevin Topollaj on 29/08/2018.
//  Copyright © 2018 Kevin Topollaj. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    // current value
    var currentValue = 0
    // create an outlet for Slider
    @IBOutlet weak var slider: UISlider!
    // target lable
    @IBOutlet weak var targetLabel: UILabel!
    var targetValue = 0
    // it will contain the score of the user
    var score = 0
    // create a label for user point
    @IBOutlet weak var scoreLabel: UILabel!
    
    // kep the round number
    var round = 0
    // the label of the round
    @IBOutlet weak var roundLabel: UILabel!
    
    // the method that executes when the UIView loads
    override func viewDidLoad() {
        super.viewDidLoad()
        currentValue = lroundf(slider.value)
        
        // call the method when the view loads
        startNewGame()
        
        // Add an image to the thumbnail
        let thumbImageNormal = #imageLiteral(resourceName: "SliderThumb-Normal") //UIImage(named: "SliderThumb-Normal")
        slider.setThumbImage(thumbImageNormal, for: .normal)
        
        let thumbImageHighlighted = #imageLiteral(resourceName: "SliderThumb-Highlighted") //UIImage(named: "SliderThumb-Highlighted")
        slider.setThumbImage(thumbImageHighlighted, for: .highlighted)
        
        // slider image
        let insets = UIEdgeInsets(top: 0, left: 14, bottom: 0, right: 14)
        // left track image
        let trackLeftImage = #imageLiteral(resourceName: "SliderTrackLeft") //UIImage(named: "SliderTrackLeft")
        let trackLeftResizable = trackLeftImage.resizableImage(withCapInsets: insets)
        slider.setMinimumTrackImage(trackLeftResizable, for: .normal)
        // right track image
        let trackRightImage = #imageLiteral(resourceName: "SliderTrackRight") //UIImage(named: "SliderTrackRight")
        let trackRightResizable = trackRightImage.resizableImage(withCapInsets: insets)
        slider.setMaximumTrackImage(trackRightResizable, for: .normal)
        
    }
    
    // method to udate a label
    func updateLabels() {
        targetLabel.text = String(targetValue)
        scoreLabel.text = String(score)
        roundLabel.text = String(round)
    }
    
    // create a method for starting a new round
    func startNewRound() {
        //increment the round
        round += 1
        // generate a random number 1 to 100
        targetValue = 1 + Int(arc4random_uniform(100))
        currentValue = 50
        slider.value = Float(currentValue)
        
        updateLabels()
    }

    @IBAction func showAlert() {
        
        // get the difference
        let difference = abs(currentValue - targetValue)
        
        // calc points
        var points = 100 - difference
        
        // contins title of the message for the user
        let title: String
        if difference == 0 {
            title = "Perfect!"
            points += 100
        } else if difference < 5 {
            title = "You almost had it!"
            if difference == 1 {
                points += 50
            }
        } else if difference < 10 {
            title = "Pretty good!"
        } else {
            title = "Try harder!"
        }
        
        // asign user points
        score += points
        
        // the message that displays the points
        let message = "You scored: \(points)"
        
        // create an alert
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        // create the button that will aperar on the alert and it will execute a closure when the button event is executed
        let action = UIAlertAction(title: "OK", style: .default, handler: {
            action in
            self.startNewRound()
        })
        
        // add the button (action) to the alert
        alert.addAction(action)
        
        // present the alert
        present(alert, animated: true, completion: nil)
        
    }

    @IBAction func startNewGame() {
        score = 0
        round = 0
        startNewRound()
    }
    
    
    @IBAction func sliderMoved(_ slider: UISlider) {
        // asign it to the global variable
        currentValue = lroundf(slider.value)
    }

}

