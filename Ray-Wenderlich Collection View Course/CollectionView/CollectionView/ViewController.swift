//
//  ViewController.swift
//  CollectionView
//
//  Created by Kevin Topollaj on 03/10/2018.
//  Copyright © 2018 Kevin Topollaj. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var addButton: UIBarButtonItem!
    @IBOutlet private weak var deleteButton: UIBarButtonItem!
    
    
    
    var collectionData = ["1 🚨", "2 🚠", "3 💽", "4 ⌛️", "5 ⚙️", "6 📃", "7 📗", "8 🆎", "9 🚈", "10 🥇", "11 🍟", "12 🏀"]
    
    // the action of adding new data to the collection view
    @IBAction func addItem(){
        
        // to add multiple items at once
        collectionView.performBatchUpdates({
            for _ in 0 ..< 2 {
                // create a text and add it to the collectionData array
                let text = "\(collectionData.count + 1)  🙃"
                collectionData.append(text)
                
                // update the collection view with the new element at the correct position
                let indexPath = IndexPath(row: collectionData.count - 1, section: 0)
                collectionView.insertItems(at: [indexPath])
            }
        }, completion: nil)
    }
    
    // the action for deleting multiple items in the editing mode
    @IBAction func deleteSelected(){
         // find the index path for the cells that are selected for deletion
        if let selected = collectionView.indexPathsForSelectedItems{
            // get the selected items form the index path
            let items = selected.map { $0.item }.sorted().reversed()
            // remove the selected items
            for item in items {
                collectionData.remove(at: item)
            }
            // update the collection view to update the change
            collectionView.deleteItems(at: selected)
        }
        // hide the tool bar after deletion
        navigationController?.isToolbarHidden = true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // set the collection view to have 3 item per row taking into acount the spacing of 10
        let width = (view.frame.size.width - 20)  / 3
        // set the type of the collection view layout
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        // set the size of one item in the controller view layout
        layout.itemSize = CGSize(width: width, height: width)
    
        // create a new refresh control and add it to the collection view
        let refresh = UIRefreshControl()
        refresh.addTarget(self, action: #selector(self.refresh), for: .valueChanged)
        collectionView.refreshControl = refresh
        
        // add an edit bar button
        navigationItem.leftBarButtonItem = editButtonItem
        
        // hide the tool bar at the initial loading
        navigationController?.isToolbarHidden = true
    }
    
    // add the method that handels the custom editing mode
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        
        // disable the addButton when the view controller is in editing mode
        addButton.isEnabled = !editing
        
        // allow multiple cell selection
        collectionView.allowsMultipleSelection = editing
        
        // get a index path for all the selected cells than deselect them when you are done
        collectionView.indexPathsForSelectedItems?.forEach{
            collectionView.deselectItem(at: $0, animated: false)
        }
        
        // change the editing mode when the editing button is taped
        let indexPaths = collectionView.indexPathsForVisibleItems
        for indexPath in indexPaths {
            let cell = collectionView.cellForItem(at: indexPath) as! CollectionViewCell
            cell.isEditing = editing
        }
        
        // enable the delete button when it is in the editing mode
        deleteButton.isEnabled = isEditing
        // if we are not in the editing mode hide the tool bar
        if !editing {
            navigationController?.isToolbarHidden = true
        }
    }
    
    // create a pull to refresh function
    @objc func refresh(){
        addItem()
        collectionView?.refreshControl?.endRefreshing()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        //check for the segue identifier
        if segue.identifier == "DetailSegue" {
            // check if the destination is in the DetailViewController and take the current selection in collection view
            if let destination = segue.destination as? DetailViewController,
                let index = sender as? IndexPath {
                // set the text in selection property in the DetailViewController as the item selected
                destination.selection = collectionData[index.row]
            }
        }
    }

}

// add two main protocols for the Collection View
extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    //*** The required methods by the UICollectionViewDataSource Protocol
    
    // the number of items that the collection view will display
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionData.count
    }
    
    // create an instance of the cell and tell it to display the data that you want
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // create the reusable cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as! CollectionViewCell
        
        // set the cell label
        cell.titleLabel.text = collectionData[indexPath.row]
        // set the state of the cell when it is in the editing mode
        cell.isEditing = isEditing
        
        return cell
    }
    
    //*** Delegate Protocol methods
    
    // handels the selection of an item in the collection view
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        // perform the segue when you are not on the editing mode
        if !isEditing{
            // call the perform segue that is used when we create a Manual Segue between two ViewControllers
            performSegue(withIdentifier: "DetailSegue", sender: indexPath)
        }else {
            // show the toolbar when a cell is selected
            navigationController?.isToolbarHidden = false
        }
    }
    
    // handels the deselection of a cell in the collection view
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        // if it is in the editing mode and the selected cell are 0 than hide the tool bar
        if isEditing{
            if let selected = collectionView.indexPathsForSelectedItems,
                selected.count == 0{
                navigationController?.isToolbarHidden = true
            }
        }
    }
    
}

