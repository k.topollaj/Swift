//
//  DetailViewController.swift
//  CollectionView
//
//  Created by Kevin Topollaj on 04/10/2018.
//  Copyright © 2018 Kevin Topollaj. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    // holds the text
    var selection: String!
    
    // outlet that will display the text
    @IBOutlet private weak var detailsLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        detailsLabel.text = selection
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
