//
//  CollectionViewCell.swift
//  CollectionView
//
//  Created by Kevin Topollaj on 04/10/2018.
//  Copyright © 2018 Kevin Topollaj. All rights reserved.
//

import UIKit

// The class that will customize the cell in the collection view

class CollectionViewCell: UICollectionViewCell {
    
    // Outlets
    
    // for the cell label
    @IBOutlet weak var titleLabel: UILabel!
    // to select the cell when it is in edit mode
    @IBOutlet weak var selectionImage: UIImageView!
    
    // track if the cell is in the editing mode
    var isEditing: Bool = false {
        didSet{
            selectionImage.isHidden = !isEditing
        }
    }
    
    // toggle the image between checked and unchecked version when the cell is selected in the editing mode
    override var isSelected: Bool{
        didSet{
            if isEditing {
                selectionImage.image = isSelected ? UIImage(named: "Checked") : UIImage(named: "Unchecked")
            }
        }
    }
}
