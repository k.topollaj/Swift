//
//  SectionHeader.swift
//  CollectionView
//
//  Created by Kevin Topollaj on 04/10/2018.
//  Copyright © 2018 Razeware. All rights reserved.
//

import UIKit

class SectionHeader: UICollectionReusableView {
    
    // outlet for the label
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var flagImage: UIImageView!
    @IBOutlet private weak var countLabel: UILabel!
    
    var section: Section! {
        didSet{
            titleLabel.text = section.title
            flagImage.image = UIImage(named: section.title ?? "")
            countLabel.text = "\(section.count)"
        }
    }
    
    // var to hold the title
    var title: String? {
        didSet {
            titleLabel.text = title
        }
    }
}
