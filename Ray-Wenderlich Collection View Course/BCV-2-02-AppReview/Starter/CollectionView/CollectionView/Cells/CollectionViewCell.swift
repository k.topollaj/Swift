import UIKit

class CollectionViewCell: UICollectionViewCell {
	@IBOutlet private weak var titleLabel:UILabel!
	@IBOutlet private weak var selectionImage:UIImageView!
    // image outlet
    @IBOutlet private weak var mainImage: UIImageView!
	
	var isEditing: Bool = false {
		didSet {
			selectionImage.isHidden = !isEditing
		}
	}
    
    // a property that will hold the data for the image displayed
    var parkImg: Park? {
        didSet {
            if let park = parkImg {
                mainImage.image = UIImage(named: park.photo)
                // set the label title to get the park name
                titleLabel.text = park.name
            }
        }
    }
	
    // remove an image displayed when an cell is reused
    override func prepareForReuse() {
        mainImage.image = nil
    }
    
	override var isSelected: Bool {
		didSet {
			if isEditing {
				selectionImage.image = isSelected ? UIImage(named: "Checked") : UIImage(named: "Unchecked")
			}
		}
	}
}
