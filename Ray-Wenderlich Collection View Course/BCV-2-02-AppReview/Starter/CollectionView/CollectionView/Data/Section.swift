//
//  Section.swift
//  CollectionView
//
//  Created by Kevin Topollaj on 04/10/2018.
//  Copyright © 2018 Razeware. All rights reserved.
//

import Foundation

class Section {
    
    var title: String?
    var count = 0
    
}
