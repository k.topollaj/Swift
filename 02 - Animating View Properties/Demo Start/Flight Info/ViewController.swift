import UIKit
import QuartzCore


//MARK: ViewController
class ViewController: UIViewController {
    
    @IBOutlet var bgImageView: UIImageView!
    
    @IBOutlet var summaryIcon: UIImageView!
    @IBOutlet var summary: UILabel!
    
    @IBOutlet var flightNr: UILabel!
    @IBOutlet var gateNr: UILabel!
    @IBOutlet var departingFrom: UILabel!
    @IBOutlet var arrivingTo: UILabel!
    @IBOutlet var planeImage: UIImageView!
    
    @IBOutlet var flightStatus: UILabel!
    @IBOutlet var statusBanner: UIImageView!
    
    var snowView: SnowView!
    
    //MARK:- animations
    func fade(toImage: UIImage, showEffects: Bool) {
        //TODO: Create a crossfade animation for the background
        // 1. Create & set up temporary view
        let tempView = UIImageView(frame: bgImageView.frame)
        tempView.image = toImage
        tempView.alpha = 0.0
        // possition the temp Image
        tempView.center.y += 20
        tempView.bounds.size.width = bgImageView.bounds.width * 1.3
        // insert the overlay
        bgImageView.superview?.insertSubview(tempView, aboveSubview: bgImageView)
        
        
        UIImageView.animate(
            withDuration: 0.5,
            animations: {
                // 2. Fade temporary view in
                tempView.alpha = 1.0
                tempView.center.y -= 20
                tempView.bounds.size = self.bgImageView.bounds.size
        },
            completion: {_ in
                // 3. Update the background view & remove the temporary view
                self.bgImageView.image = toImage
                tempView.removeFromSuperview()
        }
        )
        
        
        
        //TODO: Create a fade animation for snowView
        UIView.animate(
            withDuration: 1.0,
            delay: 0.0,
            options: .curveEaseOut,
            animations: {
                self.snowView.alpha = showEffects ? 1.0 : 0.0
        },
            completion: {_ in
                
        }
        )
    }
    
    func moveLabel(label: UILabel, text: String, offset: CGPoint) {
        //TODO: Animate a label's translation property
        
        // 1. Create & set the new temp label
        let tempLabel = duplicateLabel(label: label)
        tempLabel.text = text
        
        tempLabel.transform = CGAffineTransform(translationX: offset.x, y: offset.y) // transform label
        tempLabel.alpha = 0.0 // fade the temp label
        view.addSubview(tempLabel) // add temp label as a subview in the view
        
        // 2. Fade out and translate the real label
        UIView.animate(
            withDuration: 0.5,
            delay: 0.0,
            options: .curveEaseIn,
            animations: {
                // fade and translate
                label.transform = CGAffineTransform(translationX: offset.x, y: offset.y)
                label.alpha = 0.0
        },
            completion: nil
        )
        
        // 3. Fade in and translate the temp label
        UIView.animate(
            withDuration: 0.25,
            delay: 0.2,
            options: .curveEaseIn,
            animations: {
                tempLabel.transform = .identity // reset the transform
                tempLabel.alpha = 1.0
        },
            completion: {_ in
                // 4. Update the real label and remove the temp label
                label.text = text
                label.alpha = 1.0
                label.transform = .identity
                tempLabel.removeFromSuperview()
        }
        )
        
        
        
    }
    
    func cubeTransition(label: UILabel, text: String) {
        //TODO: Create a faux rotating cube animation
        
        // 1. Create & set a temp label
        let tempLabel = duplicateLabel(label: label)
        tempLabel.text = text
        
        let tempLabelOffset = label.frame.size.height / 2.0
        let scale = CGAffineTransform(scaleX: 1.0, y: 0.1)
        let translate = CGAffineTransform(translationX: 0.0, y: tempLabelOffset)
        
        // combine the 2 transform properties with concatenating
        tempLabel.transform = scale.concatenating(translate)
        // add the temp label to the view hierarcy
        label.superview?.addSubview(tempLabel)
        
        UIView.animate(
            withDuration: 0.5,
            delay: 0.0,
            options: .curveEaseOut,
            animations: {
                
                // 2. Scale temp label down & translate up
                tempLabel.transform = .identity
                // 3. Scale real label down & translate up
                label.transform = scale.concatenating(translate.inverted())
            },
            completion: { _ in
                
                // 4. Update the real label's text and reset its transform
                label.text = tempLabel.text
                label.transform = .identity
                // 5. Remove temp label
                tempLabel.removeFromSuperview()
            }
        )
    }
    
    
    func planeDepart() {
        //TODO: Animate the plane taking off and landing with keyframe animation
        
        // 1.Store the plain center value
        let originalCenter = planeImage.center
        // 2. Create a new keyframe animation
        UIView.animateKeyframes(
            withDuration: 1.5,
            delay: 0.0,
            animations: {
                // 3. Move the plain to the right and up
                UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 0.25, animations: {
                    self.planeImage.center.x += 80.0
                    self.planeImage.center.y -= 10.0
                })
                // 4. Rotate the plain
                UIView.addKeyframe(withRelativeStartTime: 0.1, relativeDuration: 0.4, animations: {
                    self.planeImage.transform = CGAffineTransform(rotationAngle: -.pi / 8.0)
                })
                // 5. Move the plain to the right & up offscreen, fade out
                UIView.addKeyframe(withRelativeStartTime: 0.25, relativeDuration: 0.25, animations: {
                    self.planeImage.center.x += 100.0
                    self.planeImage.center.y -= 50.0
                    self.planeImage.alpha = 0.0
                })
                // 6. Move plain of the left side off screen, reset transform and height
                UIView.addKeyframe(withRelativeStartTime: 0.51, relativeDuration: 0.01, animations: {
                    self.planeImage.transform = .identity
                    self.planeImage.center = CGPoint(x: 0.0, y: originalCenter.y)
                })
                // 7. Move plain back to the original position and fade in
                UIView.addKeyframe(withRelativeStartTime: 0.55, relativeDuration: 0.45, animations: {
                     self.planeImage.alpha = 1.0
                    self.planeImage.center = originalCenter
                })
            },
            completion: nil
        )
        
    }
    
    func changeSummary(to summaryText: String) {
        //TODO: Animate the summary text
        UIView.animateKeyframes(
            withDuration: 1.0,
            delay: 0.0,
            animations: {
                // 1. Move the label up and offscreen
                UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 0.45, animations: {
                    self.summary.center.y -= 100.0
                })
                // 2. Bring it back Down
                UIView.addKeyframe(withRelativeStartTime: 0.5, relativeDuration: 0.45, animations: {
                    self.summary.center.y += 100.0
                })
                
            },
            completion: nil
        )
        
        // 3. change the summary text
        delay(seconds: 0.5) {
            self.summary.text = summaryText
        }
    }
    
    
    //MARK:- custom methods
    
    func changeFlight(to data: FlightData, animated: Bool = false) {
        // populate the UI with the next flight's data
        
        
        

        
        
        if animated {
            //TODO: Call your animation
            // 4. call the func that holds the animation
            fade(
                toImage: UIImage(named: data.weatherImageName)!,
                showEffects: data.showWeatherEffects
            )
            
            // 5. call the moveLabel func that holds the animation
            let offsetDeparting = CGPoint(
                x: data.showWeatherEffects ? -80.0 : 80.0,
                y: 0.0
            )
            let offsetArriving = CGPoint(
                x: 0.0,
                y: data.showWeatherEffects ? 50.0 : -50.0
            )
            
            moveLabel(label:  departingFrom, text: data.departingFrom, offset: offsetDeparting)
            moveLabel(label: arrivingTo, text: data.arrivingTo, offset: offsetArriving)
            
            // 6. call the cubeTransition func
            cubeTransition(label: flightStatus, text: data.flightStatus)
            cubeTransition(label: flightNr, text: data.flightNr)
            cubeTransition(label: gateNr, text: data.gateNr)
            
            // 8. call the planeDepart func
            planeDepart()
            
            changeSummary(to: data.summary)
            
        } else {
            bgImageView.image = UIImage(named: data.weatherImageName)
            // to not show the snow when we lanch the application
            snowView.isHidden = !data.showWeatherEffects
            
            departingFrom.text = data.departingFrom
            arrivingTo.text = data.arrivingTo
            
            flightNr.text = data.flightNr
            gateNr.text = data.gateNr
            flightStatus.text = data.flightStatus
            
            summary.text = data.summary
        }
        
        // schedule next flight
        delay(seconds: 3.0) {
            self.changeFlight(to: data.isTakingOff ? parisToRome : londonToParis, animated: true)
        }
    }
    
    //MARK:- view controller methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //adjust UI
        summary.addSubview(summaryIcon)
        summaryIcon.center.y = summary.frame.size.height/2
        
        //add the snow effect layer
        snowView = SnowView(frame: CGRect(x: -150, y:-100, width: 300, height: 50))
        let snowClipView = UIView(frame: view.frame.offsetBy(dx: 0, dy: 50))
        snowClipView.clipsToBounds = true
        snowClipView.addSubview(snowView)
        view.addSubview(snowClipView)
        
        //start rotating the flights
        changeFlight(to: londonToParis, animated: false)
    }
    
    
    //MARK:- utility methods
    func delay(seconds: Double, completion: @escaping ()-> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds, execute: completion)
    }
    
    func duplicateLabel(label: UILabel) -> UILabel {
        let newLabel = UILabel(frame: label.frame)
        newLabel.font = label.font
        newLabel.textAlignment = label.textAlignment
        newLabel.textColor = label.textColor
        newLabel.backgroundColor = label.backgroundColor
        return newLabel
    }
}
