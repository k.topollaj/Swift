//
//  ImageViewController.swift
//  Cassini
//
//  Created by Kevin Topollaj on 21/09/2018.
//  Copyright © 2018 Kevin Topollaj. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController, UIScrollViewDelegate {
    
    // Model
    var imageURL: URL? {
        didSet {
            // set the computed property image to nil
            image = nil
            
            // it will check if the view is on screen to start fetching the image
            if view.window != nil {
                fetchImage()
            }
        }
    }
    
    // image object
    var imageView = UIImageView()
    
    // Create a computed property that every time you get or set the image it's going to be from the imageView
    private var image: UIImage? {
        get {
            return imageView.image
        }
        // when we set an image
        set {
            imageView.image = newValue
            
            // the size that will fit this image the best
            imageView.sizeToFit()
            
            // set the contentSize (use optional chaining because the prepare for segue happens before the scroll view outlet)
            scrollView?.contentSize = imageView.frame.size
            
            // stop the spinner after the image is set
            spinner?.stopAnimating()
        }
    }
    
    // we still need to load the image when the view appears
    
    // At this point we know that we are on screen
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // if the image view is still nil we need to fetch the image
        if imageView.image == nil {
            fetchImage()
        }
    }
    
    // create an outlet to connect the spinner
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    // connect the ScrollView
    @IBOutlet weak var scrollView: UIScrollView! {
        didSet {
            
            //1. add the Zoom properties
            scrollView.minimumZoomScale = 1/25
            scrollView.maximumZoomScale = 1.0
            
            // 2. Set the delegate
            scrollView.delegate = self
            
            // add imageView as the subview
            scrollView.addSubview(imageView)
        }
    }
    
    // 3. add the method for Zooming
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        // the subview in the content area that we want to transform when we pinch
        return imageView
    }
    
    
    // this image might be on the network
    private func fetchImage() {
        
        // check if the image is not nil
        if let url = imageURL {
            
            // start animate the spinner
            spinner.startAnimating()
            
            // will take this code and run it in to another queue when the user wants to open an image
            DispatchQueue.global(qos: .userInitiated).async { [weak self] in
                // get the image from a URL using Data but it may throw an error
                // and if it fails it will return nil
                let urlContent = try? Data(contentsOf: url)
                
                // Add it to the main queue because it contains UI activity
                DispatchQueue.main.async {
                    // if i can get the imageData i can set the image from the computed property
                    // and check to see if the url is the one we requested
                    if let imageData = urlContent, url == self?.imageURL {
                        // we don't want self to be contained in the heap by this closure
                        self?.image = UIImage(data: imageData)
                    }
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // if the time to load has passed show a sample image (stanford image/ oval.jpg)
//        if imageURL == nil {
//            // set the Model
//            imageURL = DemoURLs.stanford
//        }
        
        
    }
}
