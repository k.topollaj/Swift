//
//  CassiniViewController.swift
//  Cassini
//
//  Created by Kevin Topollaj on 26/09/2018.
//  Copyright © 2018 Kevin Topollaj. All rights reserved.
//

import UIKit

class CassiniViewController: UIViewController {

    

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // get the identifier to chose witch image to show
        if let identifier = segue.identifier {
            // look in the NASA dictionary to find the name of the identifier and then we have a image to show
            if let url = DemoURLs.NASA[identifier] {
                
                // segue to show the image in ImageViewController
                if let imageVC = segue.destination.contents as? ImageViewController {
                    imageVC.imageURL = url
                    imageVC.title = (sender as? UIButton)?.currentTitle
                }
            }
        }
    }

}

// MARK: - Extension

extension UIViewController {
    var contents: UIViewController {
        if let navcon = self as? UINavigationController {
            return navcon.visibleViewController ?? self
        } else {
            return self
        }
    }
}
