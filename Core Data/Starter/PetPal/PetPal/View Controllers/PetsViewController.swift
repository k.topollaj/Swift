
import UIKit
import CoreData

class PetsViewController: UIViewController {
	@IBOutlet private weak var collectionView:UICollectionView!

    var friend: Friend!
    
    private var fetchRC: NSFetchedResultsController<Pet>!
    private var query = ""
    private let appDelegate = UIApplication.shared.delegate as! AppDelegate
    private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    private let formatter = DateFormatter()
    
    
	private var isFiltered = false
	private var filtered = [String]()
	private var selected:IndexPath!
	private var picker = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()
		picker.delegate = self
        // set up the date formater
        formatter.dateFormat = "d MMM yyyy"
    }
    
    // will refresh the displayed data
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        refresh()
    }
    
    
    // will handel data loading
    private func refresh() {
        
        // create the request
        let request = Pet.fetchRequest() as NSFetchRequest<Pet>
        
        if query.isEmpty {
            request.predicate = NSPredicate(format: "owner = %@", friend)
        } else {
            request.predicate = NSPredicate(format: "name CONTAINS[cd] %@ AND owner = %@", query, friend)
        }
        
        // sort the data
        let sort = NSSortDescriptor(key: #keyPath(Pet.name), ascending: true, selector: #selector(NSString.caseInsensitiveCompare(_:)))
        request.sortDescriptors = [sort]
        
        do {
            fetchRC = NSFetchedResultsController(fetchRequest: request, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
            
            fetchRC.delegate = self
            
            try fetchRC.performFetch()
        } catch let error as NSError {
            print("Could not fetch: \(error), \(error.userInfo)")
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	// MARK:- Actions
    
    // Will handle delete on long press gesture
    @IBAction func handleLongPress(gestureRecognizer: UIGestureRecognizer){
        // handle the longpress gesture when it has ended
        if gestureRecognizer.state != .ended{
            return
        }
        let point = gestureRecognizer.location(in: collectionView)
        if let indexPath = collectionView.indexPathForItem(at: point){
            let peet = fetchRC.object(at: indexPath)
            context.delete(peet)
            appDelegate.saveContext()
            refresh()
        }
        
    }
    
	@IBAction func addPet() {
		let data = PetData()
        let pet = Pet(entity: Pet.entity(), insertInto: context)
        pet.name = data.name
        pet.kind = data.kind
        pet.dob = data.dob as NSDate
        pet.owner = friend
        appDelegate.saveContext()
	}
}

// Collection View Delegates
extension PetsViewController: UICollectionViewDelegate, UICollectionViewDataSource {
	
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let pets = fetchRC.fetchedObjects else {
            return 0
        }
        return pets.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		
        // create the reusable cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PetCell", for: indexPath) as! PetCell
        // get the pet object
        let pet = fetchRC.object(at: indexPath)
        
        // assign the data for each part of the cell from the pet object
        
        cell.nameLabel.text = pet.name
        cell.animalLabel.text = pet.kind
        if let dob = pet.dob as Date? {
            cell.dobLabel.text = formatter.string(from: dob)
        } else {
            cell.dobLabel.text = "Unknown"
        }
        if let photo = pet.picture as Data? {
            cell.pictureImageView.image = UIImage(data: photo)
        }else{
            cell.pictureImageView.image = UIImage(named: "pet-placeholder")
        }
        
        return cell
	}
	
    
    // what will happen when the user selects a Pet cell to add a photo
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		selected = indexPath

        self.navigationController?.present(picker, animated: true, completion: nil)
    
    }
}


// Fetch Result Controller Delegate
extension PetsViewController: NSFetchedResultsControllerDelegate {
    
    // Its called when we change the data
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        let index = indexPath ?? (newIndexPath ?? nil)
        guard let cellIndex = index else {
            return
        }
        
        switch type {
        case .insert:
            collectionView.insertItems(at: [cellIndex])
        case .delete:
            collectionView.deleteItems(at: [cellIndex])
        default:
            break
        }
        
    }
    
}




// Search Bar Delegate
extension PetsViewController:UISearchBarDelegate {
	func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
		guard let txt = searchBar.text else {
			return
		}
		
        query = txt
        refresh()
        searchBar.resignFirstResponder()
        
		collectionView.reloadData()
	}
	
	func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
		query = ""
		searchBar.text = nil
		searchBar.resignFirstResponder()
		refresh()
        collectionView.reloadData()
	}
}

// Image Picker Delegates
extension PetsViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    
        // get the pet object
        let pet = fetchRC.object(at: selected)
        
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        // get the image for the pet
        let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as! UIImage
        
        // set the picture to the pet and save
        pet.picture = image.pngData() as NSData?
        appDelegate.saveContext()
        
        collectionView?.reloadItems(at: [selected])
        picker.dismiss(animated: true, completion: nil)
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
