import UIKit
import CoreData

class MainViewController: UIViewController {
	@IBOutlet private weak var collectionView:UICollectionView!
	
    // 1.
    private let appDelegate = UIApplication.shared.delegate as! AppDelegate
    private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    // we have to specify the type of data that it will return
    private var fetchedRC: NSFetchedResultsController<Friend>!
    
	private var filtered = [Friend]()
	private var isFiltered = false
	private var selected:IndexPath!
	private var picker = UIImagePickerController()
    // will contain the expression to query the data
    private var query = ""

	override func viewDidLoad() {
		super.viewDidLoad()
		picker.delegate = self
	}
    
    // 2. gets all the friends records from the data store and stores them into the friends array
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // method to load all the data with sorting
        refresh()
        // display the edit button to the user
        showEditButton()
    }
    

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}

	// MARK:- Navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "petSegue" {
			if let index = sender as? IndexPath {
				let pvc = segue.destination as! PetsViewController
				let friend = fetchedRC.object(at: index)
				pvc.friend = friend 
			}
		}
	}

	// MARK:- Actions
	@IBAction func addFriend() {
		// 3. create a friend instance
        let data = FriendData()
        // load the data from the data generator class into the new friend object
        let friend = Friend(entity: Friend.entity(), insertInto: context)
        // attributes
        friend.name = data.name
        friend.address = data.address
        friend.dob = data.dob as NSDate
        friend.eyeColor = data.eyeColor
        // save the new record
        appDelegate.saveContext()
        // method to load all the data with sorting
        refresh()
        // update the model and the view
        collectionView.reloadData()
        showEditButton()
	}
	
	// MARK:- Private Methods
	private func showEditButton() {
        // it will return an array of fetched objects
        guard let objs = fetchedRC.fetchedObjects else {
            return
        }
		if objs.count > 0 {
			navigationItem.leftBarButtonItem = editButtonItem
		}
	}
    
    // method to load all the data with sorting
    private func refresh(){
        
        // add the sorting functionality
        let request = Friend.fetchRequest() as NSFetchRequest<Friend>
        // if the query is not empty filter the fetch data
        if !query.isEmpty {
            request.predicate = NSPredicate(format: "name CONTAINS[cd] %@", query)
        }
        let sort = NSSortDescriptor(key: #keyPath(Friend.name), ascending: true, selector: #selector(NSString.caseInsensitiveCompare(_:)))
        let color = NSSortDescriptor(key: #keyPath(Friend.eyeColor), ascending: true)
        request.sortDescriptors = [color, sort]
        
        do {
            fetchedRC = NSFetchedResultsController(fetchRequest: request, managedObjectContext: context, sectionNameKeyPath: #keyPath(Friend.eyeColor), cacheName: nil)
            try fetchedRC.performFetch()
        } catch let error as NSError {
            print("Could not fetch: \(error), \(error.userInfo)")
        }
    }
}

// Collection View Delegates
extension MainViewController: UICollectionViewDelegate, UICollectionViewDataSource {
	
    // add the section method for the view controller
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return fetchedRC.sections?.count ?? 0
    }
    
    // will return nr of items in a section
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let sections = fetchedRC.sections, let objs = sections[section].objects else {
            return 0
        }
        return objs.count
	}
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        // declare the reusable view
        let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderRow", for: indexPath)
        
        // get the label from the view
        if let label = view.viewWithTag(1000) as? UILabel {
            if let friends = fetchedRC.sections?[indexPath.section].objects as? [Friend],
                let friend = friends.first {
                label.text = "Eye Color: " + friend.eyeColorString
            }
        }
        
        return view
        
    }
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FriendCell", for: indexPath) as! FriendCell
        // fetch the object at a index path
		let friend = fetchedRC.object(at: indexPath)
		cell.nameLabel.text = friend.name
        cell.addressLabel.text = friend.address!
        cell.ageLabel.text = "Age: \(friend.age)"
        cell.eyeColorView.backgroundColor = friend.eyeColor as? UIColor
        
        // display the image
        if let data = friend.photo as Data? {
            cell.pictureImageView.image = UIImage(data: data)
        }else {
            cell.pictureImageView.image = UIImage(named: "person-placeholder")
        }
        return cell
	}
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		if isEditing {
			selected = indexPath
			self.navigationController?.present(picker, animated: true, completion: nil)
		} else {
			performSegue(withIdentifier: "petSegue", sender: indexPath)
		}
	}
}

// Search Bar Delegate
extension MainViewController: UISearchBarDelegate {
	
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let txt = searchBar.text else {
			return
		}
		// creates a fetch request
        query = txt
        // method to load all the data with sorting
        refresh()
        
		searchBar.resignFirstResponder()
		collectionView.reloadData()
	}
	
	func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        query = ""
        searchBar.text = nil
		searchBar.resignFirstResponder()
        // method to load all the data with sorting
        refresh()
        collectionView.reloadData()
	}
}

// Image Picker Delegates
extension MainViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
	func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

        // Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

		let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as! UIImage
        
		let friend = fetchedRC.object(at: selected)
		// Get the image from the core data
        friend.photo = image.pngData() as NSData?
        appDelegate.saveContext()
        
		collectionView?.reloadItems(at: [selected])
		picker.dismiss(animated: true, completion: nil)
	}
}



// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
