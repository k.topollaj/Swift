//
//  ViewController.swift
//  ImageScroller
//
//  Created by Brian on 2/9/17.
//  Copyright © 2017 Razeware. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
    super.viewDidLoad()
    
        // don't add the white top bar
        scrollView.contentInsetAdjustmentBehavior = .never
        // set the frame size to be the image itself
        imageView.frame.size = (imageView.image?.size)!
        // set the delegate
        scrollView.delegate = self
        
        setZoomParametersForSize(scrollView.bounds.size)
        
        // recenter imge
        recenterImage()
    }
    
    // override the method that gets called every time a rotation happens
    override func viewWillLayoutSubviews() {
        setZoomParametersForSize(scrollView.bounds.size)
        
        // recenter imge
        recenterImage()
    }
    
    // method that will center the image
    func recenterImage(){
        // get the scrollView size
        let scrollViewSize = scrollView.bounds.size
        //get the image size
        let imageSize = imageView.frame.size
        
        // check if there is horizontal and vertical padding and if there is divide them equaly
        let horizontalSpace = imageSize.width < scrollViewSize.width ? (scrollViewSize.width - imageSize.width) / 2 : 0
        
        let verticalSpace = imageSize.height < scrollViewSize.height ? (scrollViewSize.height - imageSize.height) / 2 : 0
        
        // create the insets
        scrollView.contentInset = UIEdgeInsets(top: verticalSpace, left: horizontalSpace, bottom: verticalSpace, right: horizontalSpace)
    }
    
    
    // the image will fit the space
    func setZoomParametersForSize(_ scrollViewSize: CGSize) {
        // get the size of the image
        let imageSize = imageView.bounds.size
        // get the scale width and hight
        let widthScale = scrollViewSize.width / imageSize.width
        let heightScale = scrollViewSize.height / imageSize.height
        
        // get the smallest value
        let minScale = min(widthScale,heightScale)
        
        // add the min, max and the default zoom scale
        scrollView.minimumZoomScale = minScale
        scrollView.maximumZoomScale = 3.0
        scrollView.zoomScale = minScale
    }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
}

// Implement a delegate method
extension ViewController: UIScrollViewDelegate{
    // add the zoom support
    public func viewForZooming(in scrollView: UIScrollView) -> UIView? {
         // provide the view that will be used to zoom
        return imageView
    }
}

