

import UIKit

class TutorialViewController: UIViewController {
  
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    
    // create an array to store all the pages
    var pages = [TutorialStepViewController]()
    
  override func viewDidLoad() {
    super.viewDidLoad()
    
        // activate paging
        scrollView.isPagingEnabled = true
    
        // create our pages
        let page1 = createAndAddTutorialStep("bg_1", iconImageName: "icon_1", text: "Pet photo sharing app")
        let page2 = createAndAddTutorialStep("bg_2", iconImageName: "icon_2", text: "Pet photo sharing app")
        let page3 = createAndAddTutorialStep("bg_3", iconImageName: "icon_3", text: "Pet photo sharing app")
        let page4 = createAndAddTutorialStep("bg_4", iconImageName: "icon_4", text: "Pet photo sharing app")
        let page5 = createAndAddTutorialStep("bg_5", iconImageName: "icon_5", text: "Pet photo sharing app")
    
        // add each item in to the array
        pages = [page1, page2, page3, page4, page5]
    
        // set the constrains between the viewControllers
        let views: [String:UIView] = ["view": view, "page1": page1.view, "page2": page2.view, "page3": page3.view, "page4": page4.view, "page5": page5.view]
    
        // a dictionary for metrics
    let metrics = ["edgeMargin" : 10, "betweenMargin" : 20]
    
        // create the vertical and horizontal constrains
        let verticalConstrains = NSLayoutConstraint.constraints(withVisualFormat: "V:|[page1(==view)]|", options: [], metrics: nil, views: views)
        let horizontalContrains = NSLayoutConstraint.constraints(withVisualFormat: "H:|-edgeMargin-[page1(==view)]-betweenMargin-[page2(==view)]-betweenMargin-[page3(==view)]-betweenMargin-[page4(==view)]-betweenMargin-[page5(==view)]-edgeMargin-|", options: [.alignAllTop, .alignAllBottom], metrics: metrics, views: views)
    
        // activate the constrains
        NSLayoutConstraint.activate(verticalConstrains + horizontalContrains)
    }

  private func createAndAddTutorialStep(_ backgroundImageName: String, iconImageName: String, text: String) -> TutorialStepViewController {
    let tutorialStep = storyboard!.instantiateViewController(withIdentifier: "TutorialStepViewController") as! TutorialStepViewController
    tutorialStep.view.translatesAutoresizingMaskIntoConstraints = false
    tutorialStep.backgroundImage = UIImage(named: backgroundImageName)
    tutorialStep.iconImage = UIImage(named: iconImageName)
    tutorialStep.text = text

    scrollView.addSubview(tutorialStep.view)

    addChildViewController(tutorialStep)
    tutorialStep.didMove(toParentViewController: self)

    return tutorialStep
  }
}

extension TutorialViewController: UIScrollViewDelegate {
    // calc where the current pageControll is
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // get the page width
        let pageWidth = scrollView.bounds.width
        // get the page fraction
        let pageFraction = scrollView.contentOffset.x / pageWidth
        // based on the pageFraction number you have to round it to the next or previous pageControll
        pageControl.currentPage = Int(round(pageFraction))
    }
}
