

import UIKit

protocol MenuTableViewControllerDelegate: class {
  func menuTableViewController(_ controller: MenuTableViewController, didSelectRow row: Int)
}

class MenuTableViewController: UITableViewController {
  weak var delegate: MenuTableViewControllerDelegate?

  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    // deselect the row
    tableView.deselectRow(at: indexPath, animated: true)
    
    delegate?.menuTableViewController(self, didSelectRow: indexPath.row)
  }
}
