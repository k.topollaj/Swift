import UIKit

class SidebarViewController: UIViewController {
    var leftViewController: UIViewController!
    var mainViewController: UIViewController!
    var overlap: CGFloat!
    var scrollView: UIScrollView!
    var firstTime = true
    
    required init(coder aDecoder: NSCoder) {
        assert(false, "Use init(leftViewController:mainViewController:overlap:)")
        super.init(coder: aDecoder)!
    }
    
    init(leftViewController: UIViewController, mainViewController: UIViewController, overlap: CGFloat) {
        self.leftViewController = leftViewController
        self.mainViewController = mainViewController
        self.overlap = overlap
        
        super.init(nibName: nil, bundle: nil)
        
        self.view.backgroundColor = UIColor.black
        
        setupScrollView()
        setupViewControllers()
    }
    
    // override
    override func viewDidLayoutSubviews() {
        if firstTime {
            firstTime = false // it will only run once
            closeMenuAnimated(false)
        }
    }
    
    // create method that will close the menu
    func closeMenuAnimated(_ animated: Bool){
        scrollView.setContentOffset(CGPoint(x: leftViewController.view.frame.width, y: 0), animated: animated)
    }
    
    // check if the menu is open
    func leftMenuIsOpen() -> Bool {
        return scrollView.contentOffset.x == 0 // the menu is open
    }
    
    // open the left menu
    func openLeftMenuAnimated(_ animated: Bool){
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: animated)
    }
    
    // open and close the menu based on the conditions
    func toogleLeftAnimated(_ animated: Bool){
        if leftMenuIsOpen() {
            closeMenuAnimated(animated)
        } else {
            openLeftMenuAnimated(animated)
        }
    }
    
    
    func setupScrollView() {
        scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        /// enable paging and disable bounces
        scrollView.isPagingEnabled = true
        scrollView.bounces = false
        view.addSubview(scrollView)
        
        let horizontalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[scrollView]|", options: [], metrics: nil, views: ["scrollView": scrollView])
        let verticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|[scrollView]|", options: [], metrics: nil, views: ["scrollView": scrollView])
        NSLayoutConstraint.activate(horizontalConstraints + verticalConstraints)
    }
    
    func setupViewControllers() {
        // add the viewControllers
        addViewController(leftViewController)
        addViewController(mainViewController)
        // add shadow
        addShadowToView(mainViewController.view)
        
        // set autolayout constrains
        let views: [String: UIView] = ["left": leftViewController.view, "main": mainViewController.view, "outer": view]
        let horizontalConstrains = NSLayoutConstraint.constraints(withVisualFormat: "|[left][main(==outer)]|", options: [.alignAllTop, .alignAllBottom], metrics: nil, views: views)
        let leftWidthConstraint = NSLayoutConstraint(item: leftViewController.view, attribute: .width, relatedBy: .equal, toItem: view, attribute: .width, multiplier: 1.0, constant: -overlap)
        let verticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|[main(==outer)]|", options: [], metrics: nil, views: views)
        
        // activate the constrains
         NSLayoutConstraint.activate(horizontalConstrains + verticalConstraints + [leftWidthConstraint])
    }
    
    private func addViewController(_ viewController: UIViewController){
        // set maunal Constrains
        viewController.view.translatesAutoresizingMaskIntoConstraints = false
        // add the viewController as the child of the scroll view
        scrollView.addSubview(viewController.view)
        // set up viewController hierarchy
        addChildViewController(viewController)
        // inform the viewController that it has been moved to a parent
        viewController.didMove(toParentViewController: self)
    }
    
    // a method to add shadow to the view
    private func addShadowToView(_ destView: UIView){
        destView.layer.shadowPath = UIBezierPath(rect: destView.bounds).cgPath
        destView.layer.shadowRadius = 2.5
        destView.layer.shadowOffset = CGSize(width: 0, height: 0)
        destView.layer.shadowOpacity = 1.0
        destView.layer.shadowColor = UIColor.black.cgColor
    }
    
}
