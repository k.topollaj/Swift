//
//  ViewController.swift
//  LayoutScroll
//
//  Created by Brian on 2/18/17.
//  Copyright © 2017 Razeware. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  @IBOutlet weak var scrollView: UIScrollView!

    // create a property to my refresh control
    var refreshControl: UIRefreshControl!
    
    @objc func refreshData(sender: UIRefreshControl){
        print("refresh data")
        // after refresh tell the data that you are done with the refresh
        sender.endRefreshing()
    }
    
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // get an instance of the UIControl
    refreshControl = UIRefreshControl()
    // call the refresh control
    refreshControl.addTarget(self, action: #selector(refreshData(sender:)), for: .valueChanged)
    // add a title to our refreshControl
    refreshControl.attributedTitle = NSAttributedString(string: "Pull to Refresh")
    // add the refreshControll to the scrollView
    scrollView.refreshControl = refreshControl
    
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }

}

