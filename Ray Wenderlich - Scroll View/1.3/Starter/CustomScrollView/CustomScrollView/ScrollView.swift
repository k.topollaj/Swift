//
//  ScrollView.swift
//  CustomScrollView
//
//  Created by Kevin Topollaj on 24/09/2018.
//  Copyright © 2018 Razeware. All rights reserved.
//

import UIKit

class MyScrollView: UIView {

    // create an initializer for our gesture recognizer
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        // create the Pan Gesture for the ScrollView class and add the panView method
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panView(with:)))
        // add the gesture to the view
        addGestureRecognizer(panGesture)
    }
    
    @objc func panView(with gestureRecognizer: UIPanGestureRecognizer){
        // store the user pan
         let translation = gestureRecognizer.translation(in: self)
        // add Animation to the scroll
        UIView.animate(withDuration: 0.2) {
            // move the view to work on y acces
            self.bounds.origin.y  = self.bounds.origin.y - translation.y
        }
        // reset the translation
        gestureRecognizer.setTranslation(CGPoint.zero, in: self)
    }

}
