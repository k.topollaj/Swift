import UIKit

class RefreshItem {
    
    private var centerStart: CGPoint
    private var centerEnd: CGPoint
    // create an reference to the image view
    unowned var view: UIView
    
    // initializer
    init(view: UIView, centerEnd: CGPoint, parallaxRatio: CGFloat, sceneHeight: CGFloat) {
        self.view = view
        self.centerEnd = centerEnd
        centerStart = CGPoint(x: centerEnd.x, y: centerEnd.y + (parallaxRatio * sceneHeight))
        self.view.center = centerStart
    }
    
    func updateViewPositionForPercentage(_ percentage: CGFloat) {
        
        // where the view should be based on the percentage of the pull down
        view.center = CGPoint(
            x: centerStart.x + (centerEnd.x - centerStart.x) * percentage,
            y: centerStart.y + (centerEnd.y - centerStart.y) * percentage
        )
    }
}
