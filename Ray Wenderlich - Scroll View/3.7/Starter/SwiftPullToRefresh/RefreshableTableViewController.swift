
import UIKit

// define a height
private let refreshViewHeight: CGFloat = 200


func delayBySeconds(_ seconds: Double, delayedCode: @escaping ()->()){
    let targetTime = DispatchTime.now() + Double(Int64(Double(NSEC_PER_SEC) * seconds)) / Double(NSEC_PER_SEC)
    DispatchQueue.main.asyncAfter(deadline: targetTime){
        delayedCode()
    }
}




class RefreshableTableViewController: UITableViewController {
    
    // Create a property for the refreshView
    var refreshView: RefreshView!
    
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // add the refreshView
    refreshView = RefreshView(frame: CGRect(x: 0, y: -refreshViewHeight, width: view.bounds.width, height: refreshViewHeight), scrollView: tableView)
    
    refreshView.translatesAutoresizingMaskIntoConstraints = false
    
    // add the subview
    view.insertSubview(refreshView, at: 0)
    
    refreshView.delegate = self
  }
    
    override func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        refreshView.scrollViewWillEndDragging(scrollView, withVelocity: velocity, targetContentOffset: targetContentOffset)
    }
    
    // override the method
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // pass it to the refreshView
        refreshView.scrollViewDidScroll(scrollView)
    }
}


extension RefreshableTableViewController: RefreshViewDelegate {
    func refreshViewDidRefresh(refreshView: RefreshView) {
        delayBySeconds(3.0) {
            self.refreshView.endRefreshing()
        }
    }
}


extension RefreshableTableViewController   {
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 20
  }

  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as UITableViewCell

    cell.textLabel?.text = "Cell \(indexPath.row)"

    return cell
  }
}
