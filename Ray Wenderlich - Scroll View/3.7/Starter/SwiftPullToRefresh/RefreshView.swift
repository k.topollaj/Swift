

import UIKit

private let sceneHeight: CGFloat = 180

// create a protocol
protocol RefreshViewDelegate: class {
    func refreshViewDidRefresh(refreshView: RefreshView)
}

class RefreshView: UIView {
    
    // 1. create a reference to the scrollView
    private unowned var scrollView: UIScrollView
    // how far the user has pulled down on the scroll view
    var progressPercentage: CGFloat = 0
    // array
    var refreshItems = [RefreshItem]()
    
    // Refresh properties
    var refreshing = false
    weak var delegate: RefreshViewDelegate?
    
    var signRefreshItem: RefreshItem!
    var isSignVisible = false
    
    // 2. Initializer
    required init?(coder aDecoder: NSCoder) {
        scrollView = UIScrollView()
        super.init(coder: aDecoder)
        
        setupRefreshItems()
    }
    
    
    func setupRefreshItems(){
        // create the image view for each item
        let groundImageView = UIImageView(image: UIImage(named: "ground"))
        let buildingsImageView = UIImageView(image: UIImage(named: "buildings"))
        let sunImageView = UIImageView(image: UIImage(named: "sun"))
        let catImageView = UIImageView(image: UIImage(named: "cat"))
        let capeBackImageView = UIImageView(image: UIImage(named: "cape_back"))
        let capeFrontImageView = UIImageView(image: UIImage(named: "cape_front"))
        
        // create refreshItems from them
        refreshItems = [
            RefreshItem(view: buildingsImageView, centerEnd: CGPoint(x: bounds.midX, y: bounds.height - groundImageView.bounds.height - buildingsImageView.bounds.height / 2), parallaxRatio: 1.5, sceneHeight: sceneHeight),
            
            RefreshItem(view: sunImageView, centerEnd: CGPoint(x: bounds.width * 0.1, y: bounds.height - groundImageView.bounds.height - sunImageView.bounds.height), parallaxRatio: 3, sceneHeight: sceneHeight),
            
            RefreshItem(view: groundImageView, centerEnd: CGPoint(x: bounds.midX, y: bounds.height - groundImageView.bounds.height / 2), parallaxRatio: 0.5, sceneHeight: sceneHeight),
            
            RefreshItem(view: capeBackImageView, centerEnd: CGPoint(x: bounds.midX, y: bounds.height - groundImageView.bounds.height / 2 - capeBackImageView.bounds.height / 2), parallaxRatio: -1, sceneHeight: sceneHeight),
            
            RefreshItem(view: catImageView, centerEnd: CGPoint(x: bounds.midX, y: bounds.height - groundImageView.bounds.height / 2 - catImageView.bounds.height / 2), parallaxRatio: 1, sceneHeight: sceneHeight),
            
            RefreshItem(view: capeFrontImageView, centerEnd: CGPoint(x: bounds.midX, y: bounds.height - groundImageView.bounds.height / 2 - capeFrontImageView.bounds.height / 2), parallaxRatio: -1, sceneHeight: sceneHeight)
            
        ]
        
        // loop and add them to the subview
        for refreshItem in refreshItems {
            addSubview(refreshItem.view)
        }
        
        let signImageView = UIImageView(image: UIImage(named: "sign"))
        signRefreshItem = RefreshItem(view: signImageView, centerEnd: CGPoint(x: bounds.midX, y: bounds.height - signImageView.bounds.height / 2), parallaxRatio: 0.5, sceneHeight: sceneHeight)
        addSubview(signImageView )
    }
    
    func showSign(_ show: Bool){
        if isSignVisible == show {
            return
        }
        
        isSignVisible = show
        
        UIView.animate(
            withDuration: 0.2,
            delay: 0,
            options: [.curveEaseInOut, .beginFromCurrentState],
            animations: { () -> Void in
                self.signRefreshItem.updateViewPositionForPercentage(show ? 1 : 0)
            },
            completion: nil)
    }
    
    func updateRefreshItemPositions(){
        for refreshItem in refreshItems {
            refreshItem.updateViewPositionForPercentage(progressPercentage)
        }
    }
    
    
    init(frame: CGRect, scrollView: UIScrollView) {
        self.scrollView = scrollView
        super.init(frame: frame)
        clipsToBounds = true
        backgroundColor = UIColor.green
        
        setupRefreshItems()
    }
    
    // update the background color
    func updateBackgroundColor() {
        let value = progressPercentage * 0.7 + 0.2
        backgroundColor = UIColor(red: value, green: value, blue: value, alpha: 1.0)
        
    }
    
    // Helper method for the refresh
    func beginRefreshing() {
        refreshing = true
        showSign(false)
        UIView.animateKeyframes(
            withDuration: 0.4,
            delay: 0,
            options: UIViewKeyframeAnimationOptions(),
            animations: {
               () -> Void in self.scrollView.contentInset.top += sceneHeight
            },
            completion:{
                (_) -> Void in
            }
        )
        
        let cape = refreshItems[5].view
        let cat = refreshItems[4].view
        cape.transform = CGAffineTransform(rotationAngle: CGFloat(-Double.pi/32))
        cat.transform = CGAffineTransform(translationX: 1.0, y: 0)
        
        UIView.animate(
            withDuration: 0.2,
            delay: 0,
            options: [.repeat, .autoreverse],
            animations: { () -> Void in
                cape.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi/32))
                cat.transform = CGAffineTransform(translationX: -1.0, y: 0)
            },
            completion: nil)
        
        
        let buildings = refreshItems[0].view
        let ground = refreshItems[2].view
        
        UIView.animate(
            withDuration: 0.2,
            delay: 0,
            options: UIViewAnimationOptions(),
            animations: { () -> Void in
                ground.center.y += sceneHeight
                buildings.center.y += sceneHeight
        }, completion: nil)
        
        
        
    }
    
    func endRefreshing() {
        refreshing = false
        UIView.animateKeyframes(
            withDuration: 0.4,
            delay: 0,
            options: UIViewKeyframeAnimationOptions(),
            animations: {
                () -> Void in self.scrollView.contentInset.top -= sceneHeight
        },
            completion:{
                (_) -> Void in
        }
        )
        
        let cape = refreshItems[5].view
        let cat = refreshItems[4].view
        
        cape.transform = CGAffineTransform.identity
        cat.transform = CGAffineTransform.identity
        cape.layer.removeAllAnimations()
        cat.layer.removeAllAnimations()
    }
}


extension RefreshView: UIScrollViewDelegate {
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>){
        // start the refresh
        if !refreshing && progressPercentage == 1 {
            beginRefreshing()
            targetContentOffset.pointee.y = -scrollView.contentInset.top
            delegate?.refreshViewDidRefresh(refreshView: self)
        }
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if refreshing {
            return
        }
        
        // in order to find the percentage we have to see the height
        let refreshViewVisibleHeight = max(0, -(scrollView.contentOffset.y + scrollView.contentInset.top))
        progressPercentage = min(1, refreshViewVisibleHeight / sceneHeight)
//        print("progressPercentage : \(progressPercentage)") 
        updateBackgroundColor()
        updateRefreshItemPositions()
        showSign(progressPercentage == 1)
    }
}
