//
//  ViewController.swift
//  PullToRefresh
//
//  Created by Brian on 2/18/17.
//  Copyright © 2017 Razeware. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    // 1. Add refreshcontrol Property
    var refreshControl: UIRefreshControl!
    
    let courses = ["Beginning iOS Unit and UI Testing", "iOS Concurrency with GCD and Operations", "Networking with NSURLSession", "Introduction Custom Controls", "Beginning iOS Animation"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 2. Create an Instance of RefreshControl
        refreshControl = UIRefreshControl()
        // 4. Call
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        // 5. set default bg color
        refreshControl.backgroundColor = UIColor.green
        // 6. add the refresh control to the table view
        tableView.refreshControl = refreshControl
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // 3. Refresh the data
    @objc func refreshData(_ refreshControl: UIRefreshControl) {
        refreshControl.endRefreshing()
    }
    
    @IBAction func changedColor(_ sender: UISegmentedControl) {
        // 7. change the color of the refresh
        if sender.selectedSegmentIndex == 0 {
            refreshControl.backgroundColor = UIColor.green
        } else if sender.selectedSegmentIndex == 1 {
            refreshControl.backgroundColor = UIColor.black
        } else {
            refreshControl.backgroundColor = UIColor.blue
        }
    }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RefreshCell", for: indexPath)
        cell.textLabel?.text = courses[indexPath.row % 5]
        return cell
    }
    
}

