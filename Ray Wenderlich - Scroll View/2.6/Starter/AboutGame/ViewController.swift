import UIKit

class ViewController: UIViewController {
  @IBOutlet weak var fgScrollView: UIScrollView!
  @IBOutlet weak var felipeImageView: UIImageView!
  
  override func viewDidLoad() {
    super.viewDidLoad()

    // animate the wings
    var animationFrames = [UIImage]()
    for i in 0...3 {
      if let image = UIImage(named: "Bird\(i)") {
        animationFrames.append(image)
      }
    }

    felipeImageView.animationImages = animationFrames
    felipeImageView.animationDuration = 0.4
    felipeImageView.startAnimating()
    
    // Observers to get the keyboard event Show / Hide
    NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: .UIKeyboardWillShow, object: nil)
    
    NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: .UIKeyboardWillHide, object: nil)
  }
    
    // remove the observers when the Item is deinit
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // add the methods for selectors
    @objc func keyboardWillShow(notification: NSNotification){
        adjustInsetForKeyboardShow(show: true, notification: notification)
    }
    
    @objc func keyboardWillHide(notification: NSNotification){
        adjustInsetForKeyboardShow(show: false, notification: notification)
    }
    
    func adjustInsetForKeyboardShow(show: Bool, notification: NSNotification){
        /// get the user info from the notification
        let userInfo = notification.userInfo ?? [:]
        let keyboardFrame = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let adjustmen = (keyboardFrame.height * (show ? 1 : -1)) + 20
        
        fgScrollView.contentInset.bottom += adjustmen
        fgScrollView.scrollIndicatorInsets.bottom += adjustmen
    }

}

extension ViewController: UIScrollViewDelegate {
}

extension ViewController: UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
}
