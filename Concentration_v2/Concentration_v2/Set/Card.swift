//
//  Card.swift
//  Animated-Set
//
//  Created by Kevin Topollaj on 18/09/2018.
//  Copyright © 2018 Kevin Topollaj. All rights reserved.
//

import Foundation

// The Model of the Card

struct Card {
    
    // MARK: - Public Properties
    let numberOfSymbols: Int
    let symbol: Int
    let shading: Int
    let color: Int
    
    // MARK: - Card Features
    private typealias Features = (numberOfSymbols: Int, symbol: Int, shading: Int, color: Int)
    
    // an array of type Features
    private static var featuresFactory = [Features]()

    // build an array of Features filled with unique tuples of random values in a given range
    private static func makeFeaturesFactory() {
        let range = Card.featuresRange
        
        for numberOfSymbols in range{
            for symbol in range{
                for shading in range{
                    for color in range {
                        featuresFactory.append((numberOfSymbols, symbol, shading, color))
                    }
                }
            }
        }
    }
    
    // it will get a new Features each time and remove the last item from the featuresFactory array
    // when the featuresFactory it's empty it will create a new one
    private static func getFeatures() -> Features {
        if featuresFactory.isEmpty {
            makeFeaturesFactory()
        }
        return featuresFactory.removeLast()
    }
    
    // MARK: - Initialization
    init() {
        (numberOfSymbols, symbol, shading, color) = Card.getFeatures()
    }
}

// MARK: - Equatable
extension Card: Equatable {
    
    static func ==(lhs: Card, rhs: Card) -> Bool {
        return  lhs.numberOfSymbols == rhs.numberOfSymbols &&
            lhs.symbol == rhs.symbol &&
            lhs.shading == rhs.shading &&
            lhs.color == rhs.color
    }
}

// MARK: - Card Constant Extension
extension Card {
    private static let featuresRange = 0...2
}
