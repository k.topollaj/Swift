//
//  ConcentrationThemeChooserViewController.swift
//  Concentration_v2
//
//  Created by Kevin Topollaj on 27/08/2018.
//  Copyright © 2018 Kevin Topollaj. All rights reserved.
//

import UIKit

class ConcentrationThemeChooserViewController: UIViewController, UISplitViewControllerDelegate {

    // Create a dictionary that will contain the themes
    let themes = [
        "Sports":"⚽️🏀🏈⚾️🎾🏐🏉🎱",
        "Animals":"🐶🐱🐭🐹🐰🦊🐻🐼",
        "Faces":"😀😄😅😍😎😡😱🤢"
    ]
    
    // set ourself as a delegate in the SplitViewController so when we open the app it shows the buttons
    override func awakeFromNib() {
        splitViewController?.delegate = self
    }
    
    // secondaryViewController -> Detail, primaryViewController -> Master
    func splitViewController(
        _ splitViewController: UISplitViewController,
        collapseSecondary secondaryViewController: UIViewController,
        onto primaryViewController: UIViewController) -> Bool {
        if let cvc = secondaryViewController as? ConcentrationViewController{
            if cvc.theme == nil {
                return true
            }
        }
        return false
    }
    
    // we do it like this because we don't want a segue to happen
    @IBAction func changeTheme(_ sender: Any) {
        // if we find the detail part of ConcentrationViewController
        if let cvc = splitViewDetailConcentrationViewController {
            if let themeName = (sender as?  UIButton)?.currentTitle, let theme = themes[themeName]{
                cvc.theme = theme
            }
        }
        // push it to NavigationViewController
        else if let cvc = lastSeguedToConcentrationViewController {
            // set theme
            if let themeName = (sender as?  UIButton)?.currentTitle, let theme = themes[themeName]{
                cvc.theme = theme
            }
            // push
            navigationController?.pushViewController(cvc, animated: true)
        }else {
            performSegue(withIdentifier: "Choose Theme", sender: sender)
        }
    }
    
    private var splitViewDetailConcentrationViewController: ConcentrationViewController? {
        // it will return the Detail from SplitViewController as it is the last element
        return splitViewController?.viewControllers.last as? ConcentrationViewController
    }
    
    // create a variable that will be a pointer that will hold the theme
    private var lastSeguedToConcentrationViewController: ConcentrationViewController?
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Will check witch segue we are doing
        if segue.identifier == "Choose Theme" {
            // we make the sender to be a button and get it current title
            if let button = sender as? UIButton{
                if let themeName = button.currentTitle{
                    if let theme = themes[themeName]{
                        if let cvc = segue.destination as? ConcentrationViewController{
                            cvc.theme = theme
                            // set it every time we segue to something
                            lastSeguedToConcentrationViewController = cvc
                        }
                    }
                }
            }
        }
    }

}
