//
//  Concentration.swift
//  Concentration_v2
//
//  Created by Kevin Topollaj on 16/08/2018.
//  Copyright © 2018 Kevin Topollaj. All rights reserved.
//

// MODEL

import Foundation

struct Concentration{
    // Design the public API (application programming iterface)
    // A list of all methods and property that you are going to allow other classes to use
    
    // Initialize the Card struct as an empty Array that will contain the cards
    private(set) var cards = [ConcentrationCard]()
    
    // when it is one card face up it gets the index otherwise it is nil (Computed Property)
    private var indexOfOneAndOnlyFaceUpCard: Int? {
        get{
            // return true if the card is face up (Closures) and it's the only element in the collection
            return cards.indices.filter {cards[$0].isFaceUp}.oneAndOnly
            
//            var foundIndex: Int?
//            for index in cards.indices{
//                if cards[index].isFaceUp{
//                    if foundIndex == nil{
//                        foundIndex = index
//                    } else {   
//                        return nil
//                    }
//                }
//            }
//            return foundIndex
        }
        set{
            for index in cards.indices {
                // if we set a new value than it will be true
                cards[index].isFaceUp = (index == newValue)
            }
        }
    }
    
    // Method to chose the card based on the index
    mutating func chooseCard(at index: Int){
        // create an assertion
        assert(cards.indices.contains(index), "Concentration.chooseCard(at: \(index)): chosen index not in the cards")
        // ignore all the mached card
        if !cards[index].isMatched{
            // if they match by the index and it's not the same card you chose
            if let matchIndex = indexOfOneAndOnlyFaceUpCard, matchIndex != index{
                // check if they match
                if cards[matchIndex] == cards[index] {
                    cards[matchIndex].isMatched = true
                    cards[index].isMatched = true
                }
                // when we have 2 face up cards
                cards[index].isFaceUp = true
            }else {
                indexOfOneAndOnlyFaceUpCard = index
            }
        }
    }
    
    // create an initializer that is executed when we call the class
    init(numberOfPairOfCards: Int) {
        assert(numberOfPairOfCards > 0, "Concentration.init(\(numberOfPairOfCards)): you must have at least one pair of cards")
        var unShuffeldCards: [ConcentrationCard] = []
        // _ means ignore this
        for _ in 0..<numberOfPairOfCards {
            // initialize the Card structure
            let card = ConcentrationCard()
            // add elements to the array
            unShuffeldCards += [card,card]
        }
        
        // ***** TODO: Shuffle the cards
        while !unShuffeldCards.isEmpty {
            let randomIndex = unShuffeldCards.count.arc4random
            let card = unShuffeldCards.remove(at: randomIndex)
            cards.append(card)
        }
    }
    
}
