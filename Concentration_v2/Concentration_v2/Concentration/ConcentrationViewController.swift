//
//  ViewController.swift
//  Concentration_v2
//
//  Created by Kevin Topollaj on 16/08/2018.
//  Copyright © 2018 Kevin Topollaj. All rights reserved.
//

import UIKit

class ConcentrationViewController: UIViewController {
    
    // Create the connection from the Controller to the Model
    // It wont be initialized until something try to use it
    private lazy var game = Concentration(numberOfPairOfCards: numberOfPairsOfCards)
    
    // Computed Property that will return the pair of data
    var numberOfPairsOfCards: Int {
        return (cardButtons.count + 1) / 2
    }
    
    // instance variable (Property) that will count the flips
    // Property has to be initialized
    private(set) var flipCount = 0 {
        // Property observer that will execute each time we call the Property
        didSet{
            updateFlipCountLabel()
        }
    }
    
    // Function for adding style to the Label
    private func updateFlipCountLabel(){
        // create the attributes
        let attributes: [NSAttributedStringKey:Any] = [
            .strokeWidth : 5.0,
            .strokeColor : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        ]
        // to use them create an attributed string
        let attributedString = NSAttributedString(string: "Flips: \(flipCount)", attributes: attributes)
        
        // use the attributed String on the label
        flipCountLabel.attributedText = attributedString
    }
    
    // Outlet for the label, it creates an instance variable
    @IBOutlet private weak var flipCountLabel: UILabel! {
        didSet{
            updateFlipCountLabel()
        }
    }
    
    // Outlet Collection property
    @IBOutlet private var cardButtons: [UIButton]!
    
    // Action button method for button click
    @IBAction private func touchCard(_ sender: UIButton) {
        flipCount += 1
        
        // get the index for each button card when you click on it
        // IT'S AN OPTIONAL so we have to check if it has a value
        if let cardNumber = cardButtons.index(of: sender){
             game.chooseCard(at: cardNumber)
            
            updateViewFromModel()
        }else {
            print("the card selected is not on the cardButtons")
        }
    }
    
    // will check if the cards match
    private func updateViewFromModel() {
        // get all the indexes for the array
        if cardButtons != nil{
            for index in cardButtons.indices {
                let button = cardButtons[index] // get button index
                let card = game.cards[index]    // get card index
                if card.isFaceUp {
                    button.setTitle(emoji(for: card), for: UIControlState.normal)
                    button.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                } else {
                    button.setTitle("", for: UIControlState.normal)
                    button.backgroundColor = card.isMatched ? #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 0) : #colorLiteral(red: 0.01680417731, green: 0.1983509958, blue: 1, alpha: 1)
                }
            }
        }
    }
    
    // New controller that will let us chose theme
    var theme: String? {
        didSet{
            emojiChoices = theme ?? ""
            //reset the emoji with a dictionary
            emoji = [:]
            updateViewFromModel()
        }
    }
    
    // Create an array that will contain the emoji for each card
    private var emojiChoices =  "🦇😱🙀😈🎃👻🍭🍬🍎"
    
    // Create a Dictionary (key:value)
    private var emoji = [ConcentrationCard:String]()
    
    private func emoji(for card: ConcentrationCard) -> String {
        // if it is emty take a random element, if the emojiChoices is bigger than 0
        if emoji[card] == nil, emojiChoices.count > 0 {
            // get a random Sting index
            let randomStringIndex = emojiChoices.index(emojiChoices.startIndex, offsetBy: emojiChoices.count.arc4random)
            // remove the random sting index form the dictionary
            emoji[card] = String(emojiChoices.remove(at: randomStringIndex))
        }
        // check inside the Dictionary and get a card if it is not empty (nil)
        return emoji[card] ?? "?"
    }
}

