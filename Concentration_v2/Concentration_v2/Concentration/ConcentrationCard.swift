//
//  Card.swift
//  Concentration_v2
//
//  Created by Kevin Topollaj on 16/08/2018.
//  Copyright © 2018 Kevin Topollaj. All rights reserved.
//

import Foundation
// Class VS Struct
// No Inheritance
// Struct are value type (their value can be copied),
// Classes are reference type (their reference to a value can be copied)
struct ConcentrationCard: Hashable
{
    // making the dictionary key hashable
    var hashValue: Int {return identifier}
    
    static func ==(lhs: ConcentrationCard, rhs: ConcentrationCard) -> Bool {
        return lhs.identifier == rhs.identifier
    }
    
    var isFaceUp = false
    var isMatched = false
    private var identifier: Int
    
    // a variable that is stored with the type Card
    private static var identifierFactory = 0
    
    // a method that gets the Identifier that is unique for each Card
    private static func getUniqueIdentifier() -> Int {
        identifierFactory += 1
        return identifierFactory
    }
    
    // Initializer that will be called when the struct is called
    init() {
        self.identifier = ConcentrationCard.getUniqueIdentifier()
    }
    
}
