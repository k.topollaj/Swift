//
//  EmojiArt.swift
//  EmojiArt
//
//  Created by Kevin Topollaj on 10/10/2018.
//  Copyright © 2018 Kevin Topollaj. All rights reserved.
//

import Foundation

struct EmojiArt: Codable
{
    
    var url: URL
    var emojis = [EmojiInfo]()
    
    struct EmojiInfo: Codable {
        let x: Int
        let y: Int
        let text: String
        let size: Int
    }
    
    init?(json: Data){
        if let newValue = try? JSONDecoder().decode(EmojiArt.self, from: json) {
            self = newValue
        } else {
            return nil
        }
    }
    
    // return the data of the EmojiArt as JSON format
    var json: Data? {
        return try? JSONEncoder().encode(self)
    }
    
    init(url: URL, emojis: [EmojiInfo]) {
        self.url = url
        self.emojis = emojis
    }
    
}
