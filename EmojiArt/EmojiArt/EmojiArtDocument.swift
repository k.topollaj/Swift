//
//  Document.swift
//  EmojiArt
//
//  Created by Kevin Topollaj on 10/10/2018.
//  Copyright © 2018 Kevin Topollaj. All rights reserved.
//

import UIKit

class EmojiArtDocument: UIDocument {
    
    var emojiArt: EmojiArt?
//    var thumbnail: UIImage?
    
    // converts from the model to the data
    override func contents(forType typeName: String) throws -> Any {
        return emojiArt?.json ?? Data()
    }
    
    // converts from the data to the model
    override func load(fromContents contents: Any, ofType typeName: String?) throws {
        if let json = contents as? Data {
            //take the data and turn them into an emoji art 
            emojiArt = EmojiArt(json: json)
        }
    }
    
//    override func fileAttributesToWrite(to url: URL, for saveOperation: UIDocument.SaveOperation) throws -> [AnyHashable : Any] {
//
//        var attributes = try fileAttributesToWrite(to: url, for: saveOperation)
//        if let thumbnail = self.thumbnail{
//            attributes[URLResourceKey.thumbnailDictionaryKey] = [URLThumbnailDictionaryItem.NSThumbnail1024x1024SizeKey:thumbnail]
//        }
//
//        return attributes
//    }
}

