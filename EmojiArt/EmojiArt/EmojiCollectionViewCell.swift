//
//  EmojiCollectionViewCell.swift
//  EmojiArt
//
//  Created by Kevin Topollaj on 28/09/2018.
//  Copyright © 2018 Kevin Topollaj. All rights reserved.
//

import UIKit

class EmojiCollectionViewCell: UICollectionViewCell {
    
    // Outlets
    
    @IBOutlet weak var label: UILabel!
}
