//
//  EmojiArtViewController.swift
//  EmojiArt
//
//  Created by Kevin Topollaj on 27/09/2018.
//  Copyright © 2018 Kevin Topollaj. All rights reserved.
//

import UIKit

extension EmojiArt.EmojiInfo {
    
    init?(label: UILabel) {
        if let attributedText = label.attributedText, let font = attributedText.font {
            x = Int(label.center.x)
            y = Int(label.center.y)
            
            text = attributedText.string
            size = Int(font.pointSize)
        } else {
            return nil
        }
    }
}



class EmojiArtViewController: UIViewController, UIDropInteractionDelegate, UIScrollViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDragDelegate, UICollectionViewDropDelegate, UIPopoverPresentationControllerDelegate
{
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowDocumentInfo" {
            // get the destination
            if let destination = segue.destination.contents as? DocumentInfoViewController {
                // set the destination
                destination.document = document
                
                // set the popover delegate
                if let ppc = destination.popoverPresentationController {
                    ppc.delegate = self
                }
            }
        } else if segue.identifier == "EmbedDocumentInfo" {
            embededdDocInfo = segue.destination.contents as? DocumentInfoViewController
        }
    }
    
    
    private var embededdDocInfo: DocumentInfoViewController?
    
    
    // UIPopoverPresentationControllerDelegate method to resize for different screen
    func adaptivePresentationStyle(
        for controller: UIPresentationController,
        traitCollection: UITraitCollection) -> UIModalPresentationStyle
    {
        // don't adapt just use the popover 
        return .none
    }
    
    
    // create a special method to use the Unwind segue
    @IBAction func close(bySegue: UIStoryboardSegue){
        close()
    }
    
    // MARK: - Container View width and height
    @IBOutlet weak var embeddedDocInfoWidth: NSLayoutConstraint!
    @IBOutlet weak var embeddedDocInfoHeight: NSLayoutConstraint!
    
    
    
    // MARK: - Model

    var emojiArt: EmojiArt? {
        
        get {
            
            if let url = emojiArtBackgroundImage.url{
                
                let emojis = emojiArtView.subviews.compactMap { $0 as? UILabel}.compactMap { EmojiArt.EmojiInfo(label: $0)}
                
                return EmojiArt(url: url, emojis: emojis)
            }
            return nil
            
        }
        set {
            // reset
            emojiArtBackgroundImage = (nil, nil)
            emojiArtView.subviews.compactMap { $0 as? UILabel }.forEach { $0.removeFromSuperview() }
            
            // fetch the image
            if let url = newValue?.url {
                imageFetcher = ImageFetcher(fetch: url) { (url, image) in
                    DispatchQueue.main.async {
                        // set the background image
                        self.emojiArtBackgroundImage = (url, image)
                        // add the labels for the emojis
                        newValue?.emojis.forEach {
                            
                            let attributedText = $0.text.attributedString(withTextStyle: .body, ofSize: CGFloat($0.size))
                            
                            self.emojiArtView.addLabel(with: attributedText, centeredAt: CGPoint(x: $0.x, y: $0.y))
                        }
                    }
                }
            }
            
        }
    }
    
    
    // MARK: - Storyboard
    
    var document: EmojiArtDocument?
    
    func documentChanged() {
        // will have the auto save
        
        // tell the document to look to the model
        document?.emojiArt = emojiArt
        
        if document?.emojiArt != nil {
            // notify if there was a change
            document?.updateChangeCount(.done)
        }
    }
    
    @IBAction func close(_ sender: UIBarButtonItem? = nil) {
        documentChanged()
        
        // stop observing the emoji art view
        if let observer = emojiArtViewObserver {
            NotificationCenter.default.removeObserver(observer)
        }
        
        
//        if document?.emojiArt != nil{
//            // after save take a snapshot of the document and use it as a thumbnail
//            document?.thumbnail = emojiArtView.snapshot
//        }
        
        // dismis the view
        presentingViewController?.dismiss(animated: true) {
            // close the document and stop the notification observer
            self.document?.close { success in
                if let observer = self.documentObserver {
                    NotificationCenter.default.removeObserver(observer)
                }
                
            }
        }
        
    
    }
    
    private var documentObserver: NSObjectProtocol?
    private var emojiArtViewObserver: NSObjectProtocol?
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // create an notification that will observe when a change happens in the document
        documentObserver = NotificationCenter.default.addObserver(
            forName: UIDocument.stateChangedNotification,
            object: document,
            queue: OperationQueue.main,
            using: { notification in
                print("documentState changed to \(self.document!.documentState)")
                
                if self.document!.documentState == .normal,
                    let docInfoVC = self.embededdDocInfo{
                    
                    docInfoVC.document = self.document
                    // set the width and height constrains
                    self.embeddedDocInfoWidth.constant = docInfoVC.preferredContentSize.width
                    self.embeddedDocInfoHeight.constant = docInfoVC.preferredContentSize.height
                    
                }
        })
        
        // open the document
        document?.open { success in
            if success {
                self.title = self.document?.localizedName
                // if opening the document was successful we have to set our Model to the Model of the Document
                self.emojiArt = self.document?.emojiArt
                
                // start observing the emoji art view
                self.emojiArtViewObserver = NotificationCenter.default.addObserver(
                    forName: Notification.Name.EmojiArtViewDidChange,
                    object: self.emojiArtView,
                    queue: OperationQueue.main,
                    using: { notification in
                        self.documentChanged()
                })
                
                
            }
            
        }
    }
    
    
    @IBOutlet weak var dropZone: UIView! {
        didSet {
            // add the interaction to recive the drop
            dropZone.addInteraction(UIDropInteraction(delegate: self))
        }
    }
    
    // create an instance of EmojiArtView
    lazy var emojiArtView = EmojiArtView()
    
    // outlets for the width and height of the scroll view
    @IBOutlet weak var scrollViewWidth: NSLayoutConstraint!
    @IBOutlet weak var scrollViewHeight: NSLayoutConstraint!
    
    
    // Scroll View outlet
    @IBOutlet weak var scrollView: UIScrollView! {
        didSet {
            // set the zoom scale
            scrollView.minimumZoomScale = 0.1
            scrollView.maximumZoomScale = 5.0
            // set the delegate to the self
            scrollView.delegate = self
            // add EmojiArtView as a subview of the ScrollView
            scrollView.addSubview(emojiArtView)
        }
    }
    
    // change the Constant of the width and height constraints when you zoom
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        scrollViewHeight.constant = scrollView.contentSize.height
        scrollViewWidth.constant = scrollView.contentSize.width
    }
    
    // method that will determine the View where we will zoom
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return emojiArtView
    }
    
    private var _emojiArtBackgroundImageURL: URL?
    
    // computed property that will get the emoji image
    var emojiArtBackgroundImage: (url: URL?, image: UIImage?) {
        get {
            return (_emojiArtBackgroundImageURL, emojiArtView.backgroundImage)
        }
        set {
            
            _emojiArtBackgroundImageURL = newValue.url
            
            scrollView?.zoomScale = 1.0
            emojiArtView.backgroundImage = newValue.image
            
            // set the content size equal to the frame
            let size = newValue.image?.size ?? CGSize.zero
            emojiArtView.frame = CGRect(origin: CGPoint.zero, size: size)
            scrollView?.contentSize = size
            
            scrollViewHeight?.constant = size.height
            scrollViewWidth?.constant = size.width
            
            if let dropZone = self.dropZone, size.width > 0, size.height > 0 {
                scrollView?.zoomScale = max(dropZone.bounds.size.width / size.width, dropZone.bounds.size.height / size.height)
            }
        }
    }
    
    
    // Collection View
    
    // Model for the collection view
    // an array of strings and each string is an emoji
    var emojis = "☺️😉😩🧐🎃👍👦👴👮‍♀️🐭🐷🐍🦀🐊🌹⭐️🌥🚔".map { String($0) }
    
    @IBOutlet weak var emojiCollectionView: UICollectionView! {
        didSet {
            // add this when you create a CollectionView
            emojiCollectionView.dataSource = self
            emojiCollectionView.delegate = self
            
            // set when you do Drag interaction with the Collection View
            emojiCollectionView.dragDelegate = self
            
            // set when you do Drag interaction with the Collection View
            emojiCollectionView.dropDelegate = self
            
            // enable drag on iPhone
            emojiCollectionView.dragInteractionEnabled = true
        }
    }
    
    // add emoji property
    private var addingEmoji = false
    
    
    // Add button action
    @IBAction func addEmoji() {
        addingEmoji = true
        emojiCollectionView.reloadSections(IndexSet(integer: 0))
    }
    
    // MARK: - UICollectionViewDataSource
    
    // define two sections
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // items in the collection
        switch section {
            case 0: return 1
            case 1: return emojis.count
            default: return 0
        }
    }
    
    
    private var font: UIFont {
        return UIFontMetrics(forTextStyle: .body).scaledFont(for: UIFont.preferredFont(forTextStyle: .body).withSize(64.0))
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 1 {
            // reuse the cell
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EmojiCell", for: indexPath)
            
            // cast to use the EmojiCollectionViewCell that we created
            if let emojiCell = cell as? EmojiCollectionViewCell {
                let text = NSAttributedString(string: emojis[indexPath.item], attributes: [.font:font])
                emojiCell.label.attributedText = text
            }
            return cell
        }
        else if addingEmoji {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EmojiInputCell", for: indexPath)
            
            // add the text from the text field
            if let inputCell = cell as? TextFieldCollectionViewCell {
                // set the resignationHandeler, memory cycle warning
                inputCell.resignatinonHandeler = { [weak self, unowned inputCell] in
                    // put the emoji from the text field to the emoji list
                    if let text  = inputCell.textField.text {
                        self?.emojis = (text.map { String($0) } + self!.emojis).uniquified
                    }
                    self?.addingEmoji = false
                    // update the model with the new emojis
                    self?.emojiCollectionView.reloadData()
                }
            }
            
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddEmojiButtonCell", for: indexPath)
            return cell
        }
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if addingEmoji && indexPath.section == 0 {
            return CGSize(width: 300, height: 80)
        }else{
            return CGSize(width: 80, height: 80)
        }
    }
    
    
    // MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let inputCell = cell as? TextFieldCollectionViewCell {
            // when the user select the text field the keyboard comes up
            inputCell.textField.becomeFirstResponder()
        }
    }
    
    // Drag interaction for Collection View
    
    // begin the drag
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        
        // this is a local drag and the context of it is the Collection View
        session.localContext = collectionView
        
        return dragItems(at: indexPath)
    }
    
    // add other items to the drag
    func collectionView(_ collectionView: UICollectionView, itemsForAddingTo session: UIDragSession, at indexPath: IndexPath, point: CGPoint) -> [UIDragItem] {
        return dragItems(at: indexPath)
    }
    
    // func for draging the items in the Collection View
    private func dragItems(at indexPath: IndexPath) -> [UIDragItem] {
        // get the cell from the EmojiCollectionViewCell than the label Outlet and then the text
        if !addingEmoji, let attributedString = (emojiCollectionView.cellForItem(at: indexPath) as? EmojiCollectionViewCell)?.label.attributedText {
            // drag the item
            let dragItem = UIDragItem(itemProvider: NSItemProvider(object: attributedString))
            // if it is a local drag
            dragItem.localObject = attributedString
            
            return [dragItem]
        }else {
            return []
        }
    }
    
    // Drop interaction for Collection View
    
    // checks if the drop interaction can be handeled if it is a String
    func collectionView(_ collectionView: UICollectionView, canHandle session: UIDropSession) -> Bool {
        // we want to drag items that are Strings
        return session.canLoadObjects(ofClass: NSAttributedString.self)
    }
    
    // if the interaction can be handeled than we have to update the session
    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
        if let indexPath = destinationIndexPath, indexPath.section == 1 {
            // checks if it is a local drag in the Collection View so it can move or copy
            let isSelf = (session.localDragSession?.localContext as? UICollectionView) == collectionView
            // return the drop proposal and accept the drop by copying it
            return UICollectionViewDropProposal(operation: isSelf ? .move : .copy, intent: .insertAtDestinationIndexPath)
        }else {
            return UICollectionViewDropProposal(operation: .cancel)
        }
        
    }
    
    // performs the drop in the collection view
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        // the index path wher we are droping
        let destinationIndexPath = coordinator.destinationIndexPath ?? IndexPath(item: 0, section: 0)
        // loop in the coordinator
        for item in coordinator.items {
            // check if the drag is coming from self
            if let sourceIndexPath = item.sourceIndexPath {
                // get the attributedString
                if let attributedString = item.dragItem.localObject as? NSAttributedString {
                    
                    // it will do it as a hole operation so you dont get out of sink with your model if you will do multiple adjustments to the collection view
                    collectionView.performBatchUpdates({
                        // remove the emoji position it came from and update the new possition
                        emojis.remove(at: sourceIndexPath.item)
                        emojis.insert(attributedString.string, at: destinationIndexPath.item)
                        
                        // delete the item and than insert it to the new destination
                        collectionView.deleteItems(at: [sourceIndexPath])
                        collectionView.insertItems(at: [destinationIndexPath])
                    })
                    
                    // tell the coordinator to do the drop and animate it
                    coordinator.drop(item.dragItem, toItemAt: destinationIndexPath)
                
                }
            }
            
            // the outer case when you want to drag items to the CollectionView from outside
            else {
                // create a place holder that will be used before the data arrives
                let placeholderContext = coordinator.drop(item.dragItem, to: UICollectionViewDropPlaceholder(insertionIndexPath: destinationIndexPath, reuseIdentifier: "DropPlaceholder"))
                // get the data and than notify the placeholderContext to remove itself
                item.dragItem.itemProvider.loadObject(ofClass: NSAttributedString.self) {(provider, error) in
                    // execute it in the main queue
                    DispatchQueue.main.async {
                        // cast the provider
                        if let attributedString = provider as? NSAttributedString {
                            // change the model and update it
                            placeholderContext.commitInsertion(dataSourceUpdates: { (insertionIndexPath) in
                                self.emojis.insert(attributedString.string, at: insertionIndexPath.item)
                            })
                        }
                        // if there was an error remove the placeholder
                        else {
                            placeholderContext.deletePlaceholder()
                        }
                    }
                }
            }
        }
    }
    
    // Accepting a drop in DropZone
    
    // checks if the drop interaction can be handeled
    func dropInteraction(_ interaction: UIDropInteraction, canHandle session: UIDropSession) -> Bool {
        // we want to drag items that have an image and an url for that image
        return session.canLoadObjects(ofClass: NSURL.self) && session.canLoadObjects(ofClass: UIImage.self)
    }
    
    // if the interaction can be handeled than we have to update the session
    func dropInteraction(_ interaction: UIDropInteraction, sessionDidUpdate session: UIDropSession) -> UIDropProposal {
        // return the drop proposal and accept the drop by copying it
        return UIDropProposal(operation: .copy)
    }
    
    // Use the class provided by the Utilities
    var imageFetcher: ImageFetcher!
    
    private var suppresBadURLWarning = false
    
    // alert for image fetching
    private func presentBadURLWarning(for url: URL?){
        
        if !suppresBadURLWarning {
            // create the alert
            let alert = UIAlertController(
                title: "Image Transfer Failed",
                message: "Couldn't transfer the dropped image from its source. \nShow this warning in the future?",
                preferredStyle: .alert)
            
            // create the buttons for the alert
            alert.addAction(UIAlertAction(
                title: "Keep Warning",
                style: .default))
            
            alert.addAction(UIAlertAction(
                title: "Stop Warning",
                style: .destructive,
                handler: { action in
                    self.suppresBadURLWarning = true
            }))
            
            // present the alert
            present(alert, animated: true)
        }
    }
    
    
    // when the user removes the finger
    func dropInteraction(_ interaction: UIDropInteraction, performDrop session: UIDropSession) {
        
        // create an instance that takes a closure with the url and the image to use or the backup image
        imageFetcher = ImageFetcher() { (url, image) in
            // get in the main queue
            DispatchQueue.main.async {
                // set the background image
                self.emojiArtBackgroundImage = (url, image)
            }
        }
        
        // tell the system that i want this data for the url by calling a closure
        session.loadObjects(ofClass: NSURL.self) { (nsurls) in
            // a drag can have multiple URLs so we get the first one
            if let url = nsurls.first as? URL {
                // than fetch the url
//                self.imageFetcher.fetch(url)
                
                // remove it from the main queue
                DispatchQueue.global(qos: .userInitiated).async {
                    // fetch the image
                    if let imageData = try? Data(contentsOf: url.imageURL),
                        let image = UIImage(data: imageData){
                        // go back to the main queue
                        DispatchQueue.main.async {
                            self.emojiArtBackgroundImage = (url, image)
                            self.documentChanged()
                        }
                    } else {
                        // call the alert
                        self.presentBadURLWarning(for: url)
                        
                    }
                }
            
            }
        }
        
        // tell the system that i want this data for the image by calling a closure
        session.loadObjects(ofClass: UIImage.self) { (images) in
            // a drag can have multiple images so we get the first one
            if let image = images.first as? UIImage{
                // and use it as a backup this image
                self.imageFetcher.backup = image
            }
        }
        
    }
    
   

}
