//
//  DocumentInfoViewController.swift
//  EmojiArt
//
//  Created by Kevin Topollaj on 11/10/2018.
//  Copyright © 2018 Kevin Topollaj. All rights reserved.
//

import UIKit

class DocumentInfoViewController: UIViewController {
    
    // MARK: - Model
    var document: EmojiArtDocument? {
        didSet {
            updateUI()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateUI()
    }
    
    // date formater
    private let shortDateFormatter: DateFormatter = {
       // init a date formater
        let formatter = DateFormatter()
        // configure it
        formatter.dateStyle = .short
        formatter.timeStyle = .short
        return formatter
    }()
    
    // update all the labels to match our model
    private func updateUI(){
        if sizeLabel != nil, createdLabel != nil,
            let url = document?.fileURL,
            let attributes = try? FileManager.default.attributesOfItem(atPath: url.path ){
            sizeLabel.text = "\(attributes[.size] ?? 0) bytes"
            //get the creation date and cast it to be a date
            if let created = attributes[.creationDate] as? Date {
                createdLabel.text = shortDateFormatter.string(from: created)
            }
        }
        
        if presentationController is UIPopoverPresentationController {
            thumbnailImageView?.isHidden = true
            returnToDocumentButton?.isHidden = true
            view.backgroundColor = .clear
        }
    }
    
    
    // set the pop over content size
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        // set the popover the smallest size posible
        if let fittedSize = topLevelView?.sizeThatFits(UIView.layoutFittingCompressedSize){
            preferredContentSize = CGSize(width: fittedSize.width + 30, height: fittedSize.height + 30 )
        }
    }
    
    
    // MARK: - Outlets
    @IBOutlet weak var topLevelView: UIStackView!
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var createdLabel: UILabel!
    @IBOutlet weak var returnToDocumentButton: UIButton!
    
    // MARK: - Actions
    @IBAction func done() {
        // ask the presenting view controller to be dismissed
        presentingViewController?.dismiss(animated: true)
    }
}
