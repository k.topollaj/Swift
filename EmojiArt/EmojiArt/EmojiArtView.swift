//
//  EmojiArtView.swift
//  EmojiArt
//
//  Created by Kevin Topollaj on 27/09/2018.
//  Copyright © 2018 Kevin Topollaj. All rights reserved.
//

import UIKit

// create you own name
extension Notification.Name {
    static let EmojiArtViewDidChange = Notification.Name("EmojiArtViewDidChange")
}


class EmojiArtView: UIView, UIDropInteractionDelegate {
    
    // add the drop interaction in the initializers to drag/drop emojis in the DropZone
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        // add the drop interaction to the view itself
        addInteraction(UIDropInteraction(delegate: self))
    }
    
    // Methods for drop interaction
    func dropInteraction(_ interaction: UIDropInteraction, canHandle session: UIDropSession) -> Bool {
        return session.canLoadObjects(ofClass: NSAttributedString.self)
    }
    
    func dropInteraction(_ interaction: UIDropInteraction, sessionDidUpdate session: UIDropSession) -> UIDropProposal {
        return UIDropProposal(operation: .copy)
    }
    
    func dropInteraction(_ interaction: UIDropInteraction, performDrop session: UIDropSession) {
        // load the object of a certain class
        session.loadObjects(ofClass: NSAttributedString.self) { (providers) in
            // get the drop point
            let dropPoint = session.location(in: self)
            //create a loop for item that we will drop as an NSAttributedString array and if it is nil as an empty array
            for attributedStrind in providers as? [NSAttributedString] ?? [] {
                // add them as subview to the view
                self.addLabel(with: attributedStrind, centeredAt: dropPoint)
                // broadcast
                NotificationCenter.default.post(name: .EmojiArtViewDidChange, object: self)
            }
        }
    }
    
    // create an dictionary that will keep things in the heap
    private var labelObservations = [UIView:NSKeyValueObservation]()
    
    func addLabel(with attributedString: NSAttributedString, centeredAt point: CGPoint){
        // create an UILabel with clear background, string as an emoji, size as the emoji and center it
        let label = UILabel()
        label.backgroundColor = .clear
        label.attributedText = attributedString
        label.sizeToFit()
        label.center = point
        // add the gesture recognizer
        addEmojiArtGestureRecognizers(to: label)
        // add as a subview
        addSubview(label)
        
        labelObservations[label] = label.observe(\.center) { (label, change) in
            NotificationCenter.default.post(name: .EmojiArtViewDidChange, object: self)
        }
    }
    
    // gets called any time a View gets removed
    override func willRemoveSubview(_ subview: UIView) {
        super.willRemoveSubview(subview)
        
        if labelObservations[subview] != nil {
            labelObservations[subview] = nil
        }
    }
    

    // Property for backgroundImage
    var backgroundImage: UIImage? {
        didSet {
            // call it to tell the system that we need to draw
            setNeedsDisplay()
        }
    }
    
    
    override func draw(_ rect: CGRect) {
        // draw the image inside the bounds
        backgroundImage?.draw(in: bounds)
    }


}
