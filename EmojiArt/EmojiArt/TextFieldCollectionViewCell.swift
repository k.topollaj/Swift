//
//  TextFieldCollectionView.swift
//  EmojiArt
//
//  Created by Kevin Topollaj on 08/10/2018.
//  Copyright © 2018 Kevin Topollaj. All rights reserved.
//

import UIKit

class TextFieldCollectionViewCell: UICollectionViewCell, UITextFieldDelegate {

    @IBOutlet weak var textField: UITextField! {
        didSet{
            // set the delegate 
            textField.delegate = self
        }
    }
    
    // type of function with no parameters and that returns Void
    var resignatinonHandeler: (() -> Void)?
    
    // Text Field Delegate methods
    
    // when the user ends the edititng in the text field
    func textFieldDidEndEditing(_ textField: UITextField) {
        // add the emoji that was in the text field
        
        resignatinonHandeler?()
    }
    
    // when the user press return it hides the keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    

}
