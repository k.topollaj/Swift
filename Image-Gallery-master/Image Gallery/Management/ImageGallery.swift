import Foundation
import UIKit

class ImageGallery: Equatable {
    static func == (lhs: ImageGallery, rhs: ImageGallery) -> Bool {
        return lhs.identifier == rhs.identifier
    }
    
    var name: String!
    var images = [UIImage]()
    var imageAspectRatios = [Double]()
    
    private var identifier: Int
    
    private static var identifierFactory = 0
    
    private static func getUniqueIdentifier() -> Int {
        identifierFactory += 1
        return identifierFactory
    }

    // MARK: - Initialization
    
    init(name: String) {
        self.name = name
        self.identifier = ImageGallery.getUniqueIdentifier()
    }
    
}

