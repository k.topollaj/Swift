import UIKit

class ImageGalleryDocumentTableViewController: UITableViewController {
        
    private lazy var detailVC: ImageGalleryCollectionViewController = {
        let layout = UICollectionViewFlowLayout()
        let vc = (self.splitViewController?.viewControllers.last as? UINavigationController)?.topViewController as! ImageGalleryCollectionViewController
        return vc
    }()
        
    // MARK: - Actions
    
    @IBAction func newImageGallery(_ sender: UIBarButtonItem) {
        let name = "Untitled".madeUnique(withRespectTo: imageGalleriesNames)
        let imageGallery = ImageGallery(name: name)
        imageGalleriesNames.append(name)
        imageGalleries.append(imageGallery)
        tableView.performBatchUpdates({
            tableView.insertRows(at: [IndexPath(item: imageGalleries.count - 1, section: 0)], with: .bottom)
        })
    }
    
    // MARK: - UIView
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if splitViewController?.preferredDisplayMode != .primaryOverlay {
            splitViewController?.preferredDisplayMode = .primaryOverlay
        }
    }
    
    // MARK: - UITableViewDataSorce

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return imageGalleries.count
        case 1:
            return recentlyDeletedImageGalleries.count
        default:
            return 0
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "imageGalleryDoumentCell", for: indexPath)
        
        if let imageGalleryDoumentCell = cell as? ImageGalleryDocumentTableViewCell {
            switch indexPath.section {
            case 0:
                imageGalleryDoumentCell.textField?.text = imageGalleries[indexPath.row].name
            case 1:
                imageGalleryDoumentCell.textField?.text = recentlyDeletedImageGalleries[indexPath.row].name
            default:
                break
            }
            
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 1:
            return "Resently Deleted"
        default:
            return ""
        }
    }
    
    // MARK: - UITableViewDelegate
    
    private var isEverSelected = false
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !isEverSelected {
            let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0))
            if let cell = cell {
                cell.setSelected(false, animated: false)
            }
            isEverSelected = true
        }
        if indexPath.section == 0 {
            detailVC.currentImageGallery = imageGalleries[indexPath.row]
            detailVC.collectionView?.reloadData()
        }
    }
    
    private var atLeastOneCellIsSelected = true
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if detailVC.currentImageGallery != nil {
            let index = imageGalleries.index(of: detailVC.currentImageGallery!)
            if indexPath.row == index && indexPath.section == 0 {
                cell.setSelected(true, animated: false)
            }
        }
    }
        
    // MARK: - Editing
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if indexPath.section == 0 {
                let cell = tableView.cellForRow(at: indexPath)
                var needsToSetFirstCellSelected = false
                if let cell = cell, cell.isSelected {
                    needsToSetFirstCellSelected = true
                }
                let imageGalleryToDelete = imageGalleries.remove(at: indexPath.row)
                recentlyDeletedImageGalleries.append(imageGalleryToDelete)
                detailVC.currentImageGallery = imageGalleries.count > 0 ? imageGalleries.first : nil
                detailVC.collectionView?.reloadData()
                tableView.performBatchUpdates({
                    tableView.deleteRows(at: [indexPath], with: .fade)
                    tableView.insertRows(at: [IndexPath(row: recentlyDeletedImageGalleries.count - 1, section: 1)], with: .automatic)
                })
                if needsToSetFirstCellSelected {
                    let firstCell = tableView.cellForRow(at: IndexPath(row: 0, section: 0))
                    firstCell?.setSelected(true, animated: true)
                }
            } else if indexPath.section == 1 {
                recentlyDeletedImageGalleries.remove(at: indexPath.row)
                tableView.performBatchUpdates({
                    tableView.deleteRows(at: [indexPath], with: .fade)
                })
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        if indexPath.section == 1 {
            let undelete = UIContextualAction(style: .normal, title: "Undelete") { (action, view, completion) in
                completion(true)
                let itemToUndelete = recentlyDeletedImageGalleries.remove(at: indexPath.row)
                imageGalleries.append(itemToUndelete)
                tableView.performBatchUpdates({
                    tableView.deleteRows(at: [indexPath], with: .fade)
                    tableView.insertRows(at: [IndexPath(row: imageGalleries.count - 1, section: 0)], with: .automatic)
                })
            }
            undelete.backgroundColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
            return UISwipeActionsConfiguration(actions: [undelete])
        } else {
            return nil
        }
    }
    
    // MARK: - Segue
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if let cell = sender as? UITableViewCell {
            if tableView.indexPath(for: cell)?.section == 0 {
                return true
            } else {
                return false
            }
        } else {
            return true
        }
    }

}
