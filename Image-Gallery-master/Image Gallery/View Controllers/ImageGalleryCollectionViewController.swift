import UIKit

private let reuseIdentifier = "imageGalleryCell"

class ImageGalleryCollectionViewController: UICollectionViewController, UICollectionViewDragDelegate, UICollectionViewDropDelegate, UICollectionViewDelegateFlowLayout {

    private var selectedImage: UIImage!
        
    lazy var currentImageGallery: ImageGallery? = {
        return imageGalleries.first
    }()
    
    private var currentScale: CGFloat = 1.0
    private var notPinched = true
    
    // MARK: - ViewController Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.dragDelegate = self
        collectionView?.dropDelegate = self
        collectionView?.addGestureRecognizer(UIPinchGestureRecognizer(target: self, action: #selector(userDidPinch(_:))))
        navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
        self.title = "Image Gallery"
    }

    // MARK: - Actions
    
    @objc private func userDidPinch(_ sender: UIPinchGestureRecognizer) {
        if notPinched {
            if sender.scale > currentScale && imageWidth <= 299 {
                imageWidth += 75
            } else if sender.scale < currentScale && imageWidth >= 101 {
                imageWidth -= 75
            }
            flowLayout?.invalidateLayout()
        }
        
        if sender.state == .ended || sender.state == .failed || sender.state == .cancelled {
            notPinched = true
        } else {
            notPinched = false
        }
    }
    
    // MARK: - UICollectionViewDataSorce
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return currentImageGallery != nil ? currentImageGallery!.images.count : 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
        
        if let imageGalleryCell = cell as? ImageGalleryCollectionViewCell, let cig = currentImageGallery {
            imageGalleryCell.imageView.image = cig.images[indexPath.item]
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        session.localContext = collectionView
        return dragItems(at: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, itemsForAddingTo session: UIDragSession, at indexPath: IndexPath, point: CGPoint) -> [UIDragItem] {
        return dragItems(at: indexPath)
    }
    
    // MARK: - UICollectionViewDelegate
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cig = currentImageGallery {
            let image = cig.images[indexPath.item]
            selectedImage = image
            self.performSegue(withIdentifier: "showImage", sender: nil)
        }
    }
    
    // MARK: - UICollectionViewDropDelegate
    
    func collectionView(_ collectionView: UICollectionView, canHandle session: UIDropSession) -> Bool {
        return (session.canLoadObjects(ofClass: NSURL.self) && session.canLoadObjects(ofClass: UIImage.self)) || session.canLoadObjects(ofClass: UIImage.self)
    }
    
    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
        let isSelf = (session.localDragSession?.localContext as? UICollectionView) == collectionView
        return UICollectionViewDropProposal(operation: isSelf ? .move : .copy, intent: .insertAtDestinationIndexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        let destinationIndexPath = coordinator.destinationIndexPath ?? IndexPath(item: 0, section: 0)
        for item in coordinator.items {
            if let sourceIndexPath = item.sourceIndexPath {
                if let image = item.dragItem.localObject as? UIImage, let cig = currentImageGallery {
                    let imageAcpectRatio = cig.imageAspectRatios[sourceIndexPath.item]
                    collectionView.performBatchUpdates({
                        cig.images.remove(at: sourceIndexPath.item)
                        cig.images.insert(image, at: destinationIndexPath.item)
                        cig.imageAspectRatios.remove(at: sourceIndexPath.item)
                        cig.imageAspectRatios.insert(imageAcpectRatio, at: destinationIndexPath.item)
                        collectionView.deleteItems(at: [sourceIndexPath])
                        collectionView.insertItems(at: [destinationIndexPath])
                    })
                    coordinator.drop(item.dragItem, toItemAt: destinationIndexPath)
                }
            } else {
                let placeholderContext = coordinator.drop(
                    item.dragItem,
                    to: UICollectionViewDropPlaceholder(insertionIndexPath: destinationIndexPath, reuseIdentifier: "dropPlaceholderCell")
                )
                item.dragItem.itemProvider.loadObject(ofClass: NSURL.self) { (provider, error) in
                    DispatchQueue.main.async {
                        if let url = provider as? URL {
                            // Image fetching
                            DispatchQueue.global(qos: .userInitiated).async { [weak self] in
                                let urlContents = try? Data(contentsOf: url.imageURL)
                                DispatchQueue.main.async {
                                    if let imageData = urlContents {
                                        let image = UIImage(data: imageData)
                                        if let image = image, let cig = self?.currentImageGallery {
                                            cig.imageAspectRatios.insert(Double(image.size.width / image.size.height), at: destinationIndexPath.item)
                                            placeholderContext.commitInsertion(dataSourceUpdates: { (insertionIndexPath) in
                                                cig.images.insert(image, at: insertionIndexPath.item)
                                            })
                                        }
                                    } else {
                                        placeholderContext.deletePlaceholder()
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    
    var flowLayout: UICollectionViewFlowLayout? {
        return collectionView?.collectionViewLayout as? UICollectionViewFlowLayout
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        if let cig = currentImageGallery {
            return CGSize(width: imageWidth, height: imageWidth / cig.imageAspectRatios[indexPath.item])
        }
        return CGSize(width: 0.0, height: 0.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(15, 15, 15, 15)
    }
    
    // MARK: - ImageGalleryDocumentTableViewControllerDelegate
    
    func imageGalleryDidChange(to newImageGallery: ImageGallery) {
//        currentImageGallery = newImageGallery
//        collectionView?.reloadData()
    }
    
    // MARK: - Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let upcoming = segue.destination.contents as? ImageViewController {
            upcoming.image = selectedImage
        }
    }
    
    // MARK: - Helping
    
    private func dragItems(at indexPath: IndexPath) -> [UIDragItem] {
        if let image = (collectionView?.cellForItem(at: indexPath) as? ImageGalleryCollectionViewCell)?.imageView.image {
            let dragItem = UIDragItem(itemProvider: NSItemProvider(object: image))
            dragItem.localObject = image
            return [dragItem]
        } else {
            return []
        }
    }

}
