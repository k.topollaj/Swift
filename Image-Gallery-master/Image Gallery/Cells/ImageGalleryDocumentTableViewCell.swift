import UIKit

class ImageGalleryDocumentTableViewCell: UITableViewCell, UITextFieldDelegate {
    
    private var identifier = 0
    
    private static var identifierFactory = -1
    
    private static func getUniqueIdentifier() -> Int {
        identifierFactory += 1
        return identifierFactory
    }
        
    @IBOutlet weak var textField: UITextField! {
        didSet {
            let tap = UITapGestureRecognizer(target: self, action: #selector(doubleTapped))
            tap.numberOfTapsRequired = 2
            self.addGestureRecognizer(tap)
            textField.delegate = self
            textField.clearsOnBeginEditing = false
            textField.isUserInteractionEnabled = false
        }
    }
    
    override func awakeFromNib() {
        self.identifier = ImageGalleryDocumentTableViewCell.getUniqueIdentifier()
    }
    
    // MARK: - Actions
    
    @objc private func doubleTapped() {
        textField.isUserInteractionEnabled = true
        textField.becomeFirstResponder()
    }
    
    // MARK: - UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.text != nil && textField.text != "" {
            imageGalleriesNames[self.identifier] = textField.text!
            textField.resignFirstResponder()
            textField.isUserInteractionEnabled = false
            return true
        } else {
            return false
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.isUserInteractionEnabled = false
    }
    
}

