//
//  ViewController.swift
//  PlayingCard
//
//  Created by Kevin Topollaj on 23/08/2018.
//  Copyright © 2018 Kevin Topollaj. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    // Initialize the deck of cards
    var deck = PlayingCardDeck()

    // add a swipe gesture so we need a outlet
    @IBOutlet weak var playingCardView: PlayingCardView! {
        didSet {
            // the swipe its going to affect the model sow we handle it in the controller
            let swipe = UISwipeGestureRecognizer(target: self, action: #selector(nextCard))
            // config the swipe gesture ( swipe left or right)
            swipe.direction = [.left,.right]
            // ask the UIView to recognise this gesture
            playingCardView.addGestureRecognizer(swipe)
            
            // create a pinch gesture
            let pinch = UIPinchGestureRecognizer(target: playingCardView, action: #selector(PlayingCardView.adjustFaceCardScale(byHandlingGestureRecognizedBy: )))
            // ask the UIView to recognise the pinch
            playingCardView.addGestureRecognizer(pinch)
        }
    }
    
    // the func that will flip our cards
    @objc func nextCard() {
        if let card = deck.draw() {
            playingCardView.rank = card.rank.order
            playingCardView.suit = card.suit.rawValue
        }
    }
    
    // Action to flip the card on tap
    @IBAction func flipCard(_ sender: UITapGestureRecognizer) {
        switch sender.state {
            case .ended: playingCardView.isFaceUp = !playingCardView.isFaceUp
            default: break
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    

}

