//
//  PlayingCard.swift
//  PlayingCard
//
//  Created by Kevin Topollaj on 23/08/2018.
//  Copyright © 2018 Kevin Topollaj. All rights reserved.
//

import Foundation

struct PlayingCard: CustomStringConvertible
{
    var description: String { return "\(rank)\(suit)"}
    
    
    var suit: Suit
    var rank: Rank
    
    // create enumeration
    enum Suit: String, CustomStringConvertible
    {
        case spades = "♠️"
        case hearts = "♥️"
        case clubs = "♣️"
        case diamonds = "♦️"
        
        // all elements of Suit
        static var all = [Suit.spades,.hearts,.clubs,.diamonds]
        
        var description: String {return rawValue}
    }
    
    enum Rank: CustomStringConvertible {
        case ace // type of card
        case face(String) // face cards J, Q, K
        case numeric(Int) // card number
        
        // order of cards
        var order: Int{
            switch self {
            case .ace: return 1
            case .numeric(let pips): return pips
            case .face(let kind) where kind == "J": return 11
            case .face(let kind) where kind == "Q": return 12
            case .face(let kind) where kind == "K": return 13
            default:
                return 0
            }
        }
        
        // get all the elements
        static var all: [Rank] {
            var allRanks = [Rank.ace]
            for pips in 2...10{
                allRanks.append(Rank.numeric(pips))
            }
            allRanks += [Rank.face("J"),.face("Q"),.face("K")]
            return allRanks
        }
        
        // description
        var description: String {
            switch self {
            case .ace: return "A" // for the ace
            case .numeric(let pips): return String(pips) // for the numbers
            case .face(let kind): return kind // for the face cards
            }
        }
    }
}
