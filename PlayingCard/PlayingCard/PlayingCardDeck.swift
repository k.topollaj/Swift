//
//  PlayingCardDeck.swift
//  PlayingCard
//
//  Created by Kevin Topollaj on 23/08/2018.
//  Copyright © 2018 Kevin Topollaj. All rights reserved.
//

import Foundation

struct PlayingCardDeck {
    
    // cards in a deck
    private(set) var cards = [PlayingCard]()
    
    // Initializer
    init() {
        // go for each item in the array
        for suit in PlayingCard.Suit.all{
            for rank in PlayingCard.Rank.all{
                cards.append(PlayingCard(suit: suit, rank: rank))
            }
        }
    }
    
    // a func that will get the playing card of the deck randomly
    mutating func draw() -> PlayingCard? {
        if cards.count > 0{
            return cards.remove(at: cards.count.arc4random)
        } else{
            return nil
        }
    }
}

extension Int{
    var arc4random: Int {
        if self > 0 {
            return  Int(arc4random_uniform(UInt32(self)))
        } else if self < 0 {
            return -Int(arc4random_uniform(UInt32(abs(self))))
        } else {
            return 0
        }
    }
}
