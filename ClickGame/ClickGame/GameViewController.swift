//
//  GameViewController.swift
//  ClickGame
//
//  Created by Kevin Topollaj on 30/08/2018.
//  Copyright © 2018 Kevin Topollaj. All rights reserved.
//

import UIKit

class GameViewController: UIViewController {

    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var clickMeBtn: UIButton!
    @IBOutlet weak var hidenResultView: UIView!
    @IBOutlet weak var resultPointLabel: UILabel!
    @IBOutlet weak var resultMessageLabel: UILabel!
    
    var currentPoints = 0
    var counter = 60
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // points when the view loads
        pointsLabel.text = "\(currentPoints)"
        
        var _ = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
    }
    
    @IBAction func clickBtn(_ sender: UIButton) {
        // incrementing the points on btn click
        currentPoints += 1
        pointsLabel.text = "\(currentPoints)"
    }
    
    @objc func updateCounter() {
        if counter > 0 {
            counter -= 1
            timerLabel.text = String(counter)
        } else {
            timerLabel.text = "Time is up!"
            clickMeBtn.isHidden = true
            clickMeBtn.isEnabled = false
            hidenResultView.isHidden = false
            
            if currentPoints <= 20 {
                resultPointLabel.text = "\(currentPoints)"
                resultMessageLabel.text = "That’s Bad ☹️. Try harder !"
                hidenResultView.backgroundColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            }
            else if currentPoints >= 21 && currentPoints <= 40 {
                resultPointLabel.text = "\(currentPoints)"
                resultMessageLabel.text = "You could have done better 🙂"
                hidenResultView.backgroundColor = #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1)
            } else if currentPoints > 41 {
                resultPointLabel.text = "\(currentPoints)"
                resultMessageLabel.text = "That’s Great! 😃 Good job."
                hidenResultView.backgroundColor = #colorLiteral(red: 0, green: 0.9768045545, blue: 0, alpha: 1)
            }
        }
    }
    
    //    override func didReceiveMemoryWarning() {
    //        super.didReceiveMemoryWarning()
    //        // Dispose of any resources that can be recreated.
    //    }
    
}
