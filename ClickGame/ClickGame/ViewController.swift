//
//  ViewController.swift
//  ClickGame
//
//  Created by Kevin Topollaj on 30/08/2018.
//  Copyright © 2018 Kevin Topollaj. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

   
    // TextField outlet
    @IBOutlet weak var userNameTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // constant that can access the data of the StartViewController
        let destinationStartViewController: StartViewController = segue.destination as! StartViewController
        
        // asign the userName string the text from the text field
        destinationStartViewController.userName = userNameTextField.text!
    }
}

