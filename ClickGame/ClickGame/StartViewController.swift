//
//  StartViewController.swift
//  ClickGame
//
//  Created by Kevin Topollaj on 30/08/2018.
//  Copyright © 2018 Kevin Topollaj. All rights reserved.
//

import UIKit

class StartViewController: UIViewController {

    // create an empty string that will store user name
    var userName = String()
    
    @IBOutlet weak var userNameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // asign to label text the userName string 
        userNameLabel.text = userName
    }
}
