import UIKit

class ViewController: UIViewController {
  
  //MARK:- IB outlets
  
  @IBOutlet var tableView: UITableView!
  @IBOutlet var buttonMenu: UIButton!
  @IBOutlet var titleLabel: UILabel!
    
    // 1. Create a constraint outlet and add it to the Constrain
    @IBOutlet weak var menuHightConstraint: NSLayoutConstraint!
    @IBOutlet weak var menuButtonTrailingConstraint: NSLayoutConstraint!
  
    // #1. Create 2 outlets for the titleCenterY position open/closed
    @IBOutlet weak var titleCenterY: NSLayoutConstraint!
    @IBOutlet weak var titleCenterY_open: NSLayoutConstraint!
    
  //MARK:- further class variables
  
  var slider: HorizontalItemList!
  var menuIsOpen = false
  var items: [Int] = [5, 6, 7]
  
  //MARK:- class methods
  
  @IBAction func toggleMenu(_ sender: AnyObject) {
    menuIsOpen = !menuIsOpen

    
    // change the title label text when open and close
    titleLabel.text = menuIsOpen ? "Select Item" : "Packing List"
    view.layoutIfNeeded()
    
    // #2 Add the cases when the menu is open or not
    titleCenterY.isActive = !menuIsOpen
    titleCenterY_open.isActive = menuIsOpen
    
    //TODO: Search for a constraint and animate its multiplier
    
    // #3 iterate through the constrains and access the one you need
    titleLabel.superview?.constraints.forEach { constraint in
//        print("-> \(constraint.description) \n")
        if constraint.firstItem === titleLabel && constraint.firstAttribute == .centerX
        {
            constraint.constant = menuIsOpen ? -100.0 : 0.0
            return
        }
        
//
//        if constraint.identifier == "TitleCenterY" {
//            constraint.isActive = false
//
//            let newConstrain = NSLayoutConstraint(
//                item: titleLabel,
//                attribute: .centerY,
//                relatedBy: .equal,
//                toItem: titleLabel.superview!,
//                attribute: .centerY,
//                multiplier: menuIsOpen ? 0.67 : 1.0,
//                constant: 5.0)
//
//            newConstrain.identifier = "TitleCenterY"
//            newConstrain.isActive = true
//
//        }
 
    }
    
    
    //TODO: Build your first constraint animation!
    // 2. the menu should be 200 when is open and 60 when it is closed
    menuHightConstraint.constant = menuIsOpen ? 200.0 : 60.0
    
    menuButtonTrailingConstraint.constant = menuIsOpen ? 16.0 : 8.0
    
    // 3. Add animation with springs
    UIView.animate(
        withDuration: 1.0,
        delay:  0.0,
        usingSpringWithDamping: 0.5,
        initialSpringVelocity: 10.0,
        options: [.allowUserInteraction],
        animations:  {
            // rotate the + button
            let angle: CGFloat = self.menuIsOpen ? .pi / 4 : 0.0
            self.buttonMenu.transform = CGAffineTransform(rotationAngle: angle)
            
            self.view.layoutIfNeeded()
        },
        completion:  nil )
  }
  
    //shows an image of the item when we tap on it
  func showItem(_ index: Int) {
    let imageView = makeImageView(index: index)
    view.addSubview(imageView)
    
    // 1. Create constrains:
    // horizontal alignment constraint (center imageView in viewController view)
    let conX = imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor)
    // verticaly position ImageView in it's parent view
    let conBottom = imageView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: imageView.frame.height)
    // set the Image width
    let conWidth = imageView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.33, constant: -50.0)
    // image view to keep constrain square size ratio
    let conHeight =  imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor)
    
    // activate them all at once
    NSLayoutConstraint.activate([conX, conBottom, conWidth, conHeight])
    view.layoutIfNeeded() // update the layout before the animation starts
    
    // 2. Animate with springs
     UIView.animate(
        withDuration: 0.8,
        delay: 0.0,
        usingSpringWithDamping: 0.5,
        initialSpringVelocity: 10.0,
        animations: {
             conBottom.constant = -imageView.frame.size.height / 2
            conWidth.constant = 0.0
            self.view.layoutIfNeeded()
        },
        completion: nil
    )
    
    // 3. Animate Out : Using Transition
    delay(seconds: 1.0) {
        UIView.transition(
            with: imageView,
            duration: 1.0,
            options: [.transitionFlipFromBottom],
            animations: {
                imageView.isHidden = true
            },
            completion: {_ in
                // remove image from the View hierarchy
                imageView.removeFromSuperview()
                
            })
    }
    
    
//    UIView.animate(
//        withDuration: 0.67,
//        delay: 2.0,
//        animations: {
//            conBottom.constant = imageView.frame.size.height
//            conWidth.constant = -50.0
//            self.view.layoutIfNeeded()
//        },
//        completion: {_ in
//            // once the preview is out of the  screen area you don't wont the image
//            imageView.removeFromSuperview()
//
//        }
//    )
    
  }

    // Function that closes the menu
  func transitionCloseMenu() {
        delay(seconds: 0.35, completion: {
            self.toggleMenu(self)
        })
    
        // Add a view transition to the slider
        // 1. get the container view for the slider
        let titleBar = slider.superview!
    
        UIView.transition(
            with: titleBar,
            duration: 0.5,
            options: [
                .curveEaseOut,
                .transitionFlipFromBottom
            ],
            animations: {
                // triger the transition
                self.slider.removeFromSuperview()
            },
            completion: {_ in
                // add it back to the view hierarchy once the animation is completed
                titleBar.addSubview(self.slider)
            }
        )
	}
}

//////////////////////////////////////
//
//   Starter project code
//
//////////////////////////////////////

let itemTitles = ["Icecream money", "Great weather", "Beach ball", "Swim suit for him", "Swim suit for her", "Beach games", "Ironing board", "Cocktail mood", "Sunglasses", "Flip flops"]

// MARK:- View Controller

extension ViewController: UITableViewDelegate, UITableViewDataSource {
  func makeImageView(index: Int) -> UIImageView {
    let imageView = UIImageView(image: UIImage(named: "summericons_100px_0\(index).png"))
    imageView.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.5)
    imageView.layer.cornerRadius = 5.0
    imageView.layer.masksToBounds = true
    imageView.translatesAutoresizingMaskIntoConstraints = false
    return imageView
  }
  
  func makeSlider() {
    slider = HorizontalItemList(inView: view)
    slider.didSelectItem = {index in
      self.items.append(index)
      self.tableView.reloadData()
      self.transitionCloseMenu()
    }
    self.titleLabel.superview?.addSubview(slider)
  }
  
  // MARK: View Controller methods
  
  override func viewDidLoad() {
    super.viewDidLoad()
    makeSlider()
    self.tableView?.rowHeight = 54.0
  }
  
  // MARK: Table View methods
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return items.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as UITableViewCell
    cell.accessoryType = .none
    cell.textLabel?.text = itemTitles[items[indexPath.row]]
    cell.imageView?.image = UIImage(named: "summericons_100px_0\(items[indexPath.row]).png")
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    showItem(items[indexPath.row])
  }
  
}
