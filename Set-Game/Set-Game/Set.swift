//
//  Set.swift
//  Set-Game
//
//  Created by Kevin Topollaj on 05/09/2018.
//  Copyright © 2018 Kevin Topollaj. All rights reserved.
//

import Foundation
import GameplayKit.GKRandomSource

class Set{
    
    // Properties
    
    private(set) var deck = [Card]()
    private(set) var cardsInPlay = [Card]()
    private(set) var selectedCards = [Card]()
    private(set) var hasMatch = false
    private(set) var score = 0
    
    // Public Functions
    
    // SET initializer
    init() {
        initDeck()
        draw(numCards: 12)
    }
    
    // will select a card based on the index
    func selectCard(at index: Int) {
        hasMatch = false
        
        if (index >= 0 && index < cardsInPlay.count) {
            let card = cardsInPlay[index] // get the index of a card
            
            /* Deselect a card if it is alredy selected and
                if there are less than 3 cards selected
                else select the card
             */
            if selectedCards.count < 3 {
                if let selectedCardIndex = selectedCards.index(of: card) {
                    selectedCards.remove(at: selectedCardIndex)
                } else {
                    selectedCards.append(card)
                    hasMatch = selectedCardsAreMatch()
                }
            }
            
            /*
                This block executes when 3 cards are selected
                it checks for a match and updates the scores.
             */
            else if selectedCards.count == 3 {
                if checkForMatch() {
                    draw(numCards: 3)
                    score += 10
                } else {
                    score -= 3
                }
                
                // if the selected card was alredy matched, selectedCards should be empty
                // else it will contain the selected card index 
                selectedCards = selectedCards.contains(card) ? [] : [card]
            }
        }
    }
    
    // will deal 3 extra cards
    func dealThreeCards() {
        // if there is a match remove all cards
        if checkForMatch(){
            selectedCards.removeAll()
        }
        
        // else draw 3 more cards and update the score
        draw(numCards: 3)
        score -= 3
    }
    
    // will start a new game
    func reset() {
        deck.removeAll()
        cardsInPlay.removeAll()
        selectedCards.removeAll()
        hasMatch = false
        score = 0
        
        initDeck()
        draw(numCards: 12)
    }
    
    // Private functions
    private func initDeck() {
        for num in Card.Number.all {
            for symbol in Card.Symbols.all{
                for shading in Card.Shading.all{
                    for color in Card.Color.all{
                        deck.append(Card(number: num,
                                        symbol: symbol,
                                        shading: shading,
                                        color: color))
                    }
                }
            }
        }
        deck = shuffle(deck)
    }
    
    // the function that will draw a certain number of cards
    private func draw(numCards: Int) {
        if deck.count >= numCards && cardsInPlay.count <= 21{
            for _ in 1...numCards{
                if let drawCard = deck.popLast() {
                    cardsInPlay.append(drawCard)
                }
            }
        }
    }
    
    // will shuffle the cards to get a random deck each time
    private func shuffle(_ deck: [Card]) -> [Card] {
        return GKRandomSource.sharedRandom().arrayByShufflingObjects(in: deck) as! [Card]
    }
    
    // if selected cards are a match, remove them from the cardsInPlay
    private func checkForMatch() -> Bool {
        // hasMatch is true if the selected cards from the function are a match
        hasMatch = selectedCardsAreMatch()
        
        if hasMatch{
            for card in selectedCards {
                if let index = cardsInPlay.index(of: card) {
                    cardsInPlay.remove(at: index)
                }
            }
        }
        return hasMatch
    }
    
    // return true if selected cards are a set or return false
    private func selectedCardsAreMatch() -> Bool {
        
        // if there are less than 3 cards you can't have a set
        if selectedCards.count < 3 {
            return false
        }
        
        // get 3 cards
        let card1 = selectedCards[0]
        let card2 = selectedCards[1]
        let card3 = selectedCards[2]
        
        // check for color
        if (!((card1.color == card2.color) && (card1.color == card3.color) || (card1.color != card2.color) && (card1.color != card3.color) && (card2.color != card3.color))){
            return false
        }
        
        // check for number
        if (!((card1.number == card2.number) && (card1.number == card3.number) ||
            (card1.number != card2.number) && (card1.number != card3.number) && (card2.number != card3.number))) {
            return false
        }
        
        // check for symbol
        if (!((card1.symbol == card2.symbol) && (card1.symbol == card3.symbol) ||
            (card1.symbol != card2.symbol) && (card1.symbol != card3.symbol) && (card2.symbol != card3.symbol))) {
            return false
        }
        
        // check for shading
        if (!((card1.shading == card2.shading) && (card1.shading == card3.shading) ||
            (card1.shading != card2.shading) && (card1.shading != card3.shading) && (card2.shading != card3.shading))) {
            return false
        }
        
        return true
    }
}

