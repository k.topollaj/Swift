//
//  Card.swift
//  Set-Game
//
//  Created by Kevin Topollaj on 05/09/2018.
//  Copyright © 2018 Kevin Topollaj. All rights reserved.
//

import Foundation

// create the structure of a card
struct Card {
    
    // Properties
    let number: Number
    let symbol: Symbols
    let shading: Shading
    let color: Color
    
    // Enumerations
    enum Number: Int {
        case one = 1
        case two = 2
        case three = 3
        
        static var all: [Number] {
            return [.one, .two, .three]
        }
    }
    
    enum Symbols: Character {
        case circle = "●"
        case triangle = "▲"
        case square = "■"
        
        static var all: [Symbols] {
            return [.circle, .triangle, .square]
        }
    }
    
    enum Shading {
        case solid
        case striped
        case open
        
        static var all: [Shading] {
            return [.solid, .striped, .open]
        }
    }
    
    enum Color {
        case teal
        case pink
        case purple
        
        static var all: [Color] {
            return [.teal, .pink, .purple]
        }
    }
}


extension Card: Equatable{
    static func ==(lhs: Card, rhs: Card) -> Bool {
        return
            (lhs.number == rhs.number) &&
            (lhs.symbol == rhs.symbol) &&
            (lhs.shading == rhs.shading) &&
            (lhs.color == rhs.color)
    }
}
