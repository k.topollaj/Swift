//
//  ViewController.swift
//  Set-Game
//
//  Created by Kevin Topollaj on 05/09/2018.
//  Copyright © 2018 Kevin Topollaj. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    // Make the Controller to Model Connection
    private var setGame = Set()
    
    
    // Outlet and Actions:
    @IBOutlet private var scoreLabel: UILabel!
    @IBOutlet private var dealCardsButton: UIButton!
    @IBOutlet private var cardButtons: [UIButton]!
    
    @IBAction func touchCard(_ sender: UIButton) {
        if let cardIndex = cardButtons.index(of: sender){
            setGame.selectCard(at: cardIndex)
            updateView()
        }
    }
    
    @IBAction func dealThreeCards(_ sender: UIButton) {
        setGame.dealThreeCards()
        updateView()
    }
    
    @IBAction func newGame(_ sender: UIButton) {
        setGame.reset()
        updateView()
    }
    
    override func viewDidLoad() {
        updateView()
    }
    
    // Private Functions
    private func updateView() {
        updateScoreLabel()
        drawCardButtons()
        dealCardsButton.isEnabled = canDealMoreCards()
    }
    
    private func drawCardButtons() {
        let cardsInPlay = setGame.cardsInPlay
        
        // draw buttons for cards in play
        for index in cardsInPlay.indices {
            let card = cardsInPlay[index]
            let cardButton = cardButtons[index]
            
            cardButton.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            cardButton.setAttributedTitle(getTitleAttributesFor(card), for: .normal)
            addButtonBorders()
        }
        
        // make the rest of buttons invisible
        for index in cardsInPlay.count..<cardButtons.count{
            let cardButton = cardButtons[index]
            
            cardButton.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            cardButton.setAttributedTitle(NSAttributedString(string: ""), for: .normal)
            cardButton.layer.borderWidth = 0.0
        }
    }
    
    // return true if the user can deal more cards or false if there is no more space
    private func canDealMoreCards() -> Bool {
        return setGame.deck.count >= 3 && setGame.cardsInPlay.count <= 21
    }
    
    // returns a dictionary of attributes to properly display the text on the card buttons
    private func getTitleAttributesFor(_ card: Card) -> NSAttributedString {
        
        var attributes = [NSAttributedStringKey: Any]()
        let cardText = getCardString(from: card)
        let foregroundColor = getForegroundColor(from: card)
        
        switch card.shading {
        case .open:
            attributes[NSAttributedStringKey.strokeWidth] = 7.0
            attributes[NSAttributedStringKey.strokeColor] = foregroundColor
        case .solid:
            attributes[NSAttributedStringKey.foregroundColor] = foregroundColor.withAlphaComponent(1.0)
        case .striped:
            attributes[NSAttributedStringKey.foregroundColor] = foregroundColor.withAlphaComponent(0.35)
        }
        
        return NSAttributedString(string: cardText, attributes: attributes)
    }
    
    
    // get the text that should appear in the card button
    private func getCardString(from card: Card) -> String {
        var cardText = ""
        
        // for the numbers in the range get the right number of symbols
        for _ in 0..<card.number.rawValue {
            cardText.append(card.symbol.rawValue)
        }
        
        return cardText
    }
    
    
    // add color to the symbols of the card button
    private func getForegroundColor(from card: Card) -> UIColor {
        let color: UIColor
        
        switch card.color {
        case .pink:
            color = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        case .purple:
            color = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
        case .teal:
            color = #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1)
        }
        
        return color
    }
    
    // draw borders for selection and for the correct and incorrect matches
    private func addButtonBorders(){
        let selectedCards = setGame.selectedCards
        let cardsInPlay = setGame.cardsInPlay
        
        for index in cardsInPlay.indices{
            let card = cardsInPlay[index]
            let cardButton = cardButtons[index]
            
            if selectedCards.contains(card){
                cardButton.layer.borderWidth = 2.0
                
                if selectedCards.count < 3 {
                    cardButton.layer.borderColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
                }else{
                    cardButton.layer.borderColor = setGame.hasMatch ? #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1) : #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
                }
            } else {
                cardButton.layer.borderWidth = 0.0
            }
        }
    }
    
    // will update the score label
    private func updateScoreLabel() {
        scoreLabel.text = "Score: \(setGame.score)"
    }


}

