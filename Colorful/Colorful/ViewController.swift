//
//  ViewController.swift
//  Colorful
//
//  Created by Kevin Topollaj on 15/08/2018.
//  Copyright © 2018 Kevin Topollaj. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    // Creating the label property (Outlet)
    @IBOutlet weak var label: UILabel!
    
    // Add the label text color based on the button background color
//    @IBAction func changeColor(_ sender: UIButton) {
//        label.textColor = sender.backgroundColor
//    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showRed" {
            _ = segue.destination as? RedViewController
        } else if segue.identifier == "showGreen" {
            _ = segue.destination as? GreenViewController
        } else if segue.identifier == "showBlue" {
            _ = segue.destination as! BlueViewController
        }
    }
    

    //TODO: Add a LongPressGestureRecognizer.
    
    @IBAction func longPressGesture(_ sender: UILongPressGestureRecognizer) {
        if sender.state == .ended {
            self.view.backgroundColor = UIColor.black
        }
    }
    
}

