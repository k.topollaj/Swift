import CoreMotion

// we add a static var
// so we can get a "shared" CMMotionManager
// that we could use throughout our app
// by simply accessing CMMotionManager.shared

extension CMMotionManager {
    static var shared = CMMotionManager()
}
